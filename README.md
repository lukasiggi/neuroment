# Neuroment

Instrument detection using Neural Networks.

## Description

Neuroment detects instruments in audio data using a CNN (Convolutional Neural Network).
The CNN structure is specifically designed for this task and its requirements.

The program offers a full Neural Network package - from data generation, to model creation, training and
prediction. It also plots the predicted instrument activations using a reference audio file, and exports 
them to a CSV file.

## How to run

Simpy run `main.py`. All program parameters can be set in `config.yaml`. We recommend
**Python 3.8**, even though each version **>= 3.6** should work.

To install the dependencies, use the environment.yaml file.

### Install dependencies

This project uses the [Anaconda](https://www.anaconda.com/) package manager. To install dependencies,
make sure you have the latest version of it installed. Verify this by checking, if the command
`conda` works in your command-line. 

If it works, you can set up the environment for this project the following way:

- create a new environment with using `conda env create -f environment.yaml`
- then activate the environment using `conda activate neuroment`


## Training

This project ships with a few samples showing you how to train the network. You can train using your own data however.

The training features are calculated using the [CQT transform](https://en.wikipedia.org/wiki/Constant-Q_transform) (Constant-Q transform). The CQT generates
linear octave distances in a spectrogram, compared to e.g. a DFT with logarithmic octave 
spacing. This is especially useful for CNNs, which operate with fixed-size kernels on image data.

To train the network you need to set up your training data in `data/training/`. You can do so by first creating
directories in there, which then serve as the names of the instruments you use. As an example, to train with the instruments *Bass* and *Guitar*, 
create a `Bass/` and a `Guitar/` directory in `data/training`.

Next copy your training data in form of WAV-files into the corresponding directories. The names of the WAV-files do not matter!
As long as it is a WAV-file, it will be used. You can then start the training (and the rest of the program) by running `main.py`.

For generating training data and training the network you need to set the following configuration parameters:

- `load_cnn: False`
- `train_cnn: True`
- `generate_base_sample_batches: True`
- `generate_mix_sample_batches: True`

If you already have training data you need only the first 2 options. The latter 2 can be set to *False*.

## Notes

This project is still under development. There are some fixes left for data generation, and also the program 
structure will be a bit overhauled and simplified.

## Maintainers

- [Lukas Maier](mailto:maier.lukas1995@gmail.com)
- [Lorenz Haeusler](mailto:haeusler.lorenz@gmail.com)