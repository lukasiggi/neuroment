"""
Neuroment - Instrument detection using Convolutional Neural Networks
---
To run the project simply run this main script. All configuration parameters for the project can be changed
in config.yaml. Packages can be installed using environment.yaml.

Authors:
- Lukas Maier, maier.lukas1995@gmail.com
- Lorenz Haeusler, haeusler.lorenz@gmail.com

This project is licensed under the GNU GPLv3 license.
See the LICENSE file or <https://www.gnu.org/licenses/> for the full license text.
"""

import numpy as np
import os
from pprint import pprint
import sys
from tensorflow.keras.models import load_model
import yaml

import src


# change working dir to dir of this file if necessary
if os.path.dirname(__file__) != "":
    os.chdir(os.path.dirname(__file__))

# check if there was a config file parsed on command line
if len(sys.argv) >= 2:
    config_file = sys.argv[1]
else:
    config_file = "config.yaml"

# get config parameters
with open(config_file, "r") as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    cfg = src.process_config(cfg)

try:
    print("Slurm job ID: %s" % int(os.environ["SLURM_JOB_ID"]))
    # deactivate plotting because we're on a headless system
    cfg["do_plot"] = False
    cfg["verbose"] = 2  # don't display progress bar
except:
    print("Did not detect slurm job ID.")
    cfg["verbose"] = 1  # display progress bar

# we print our cfg parameters s.t. we can easily look them up in the log later
pprint(cfg)

# Get instrument list
if cfg["read_instrument_list"]:
    instrument_list = src.InstrumentList(cfg["read_instrument_list_from"])
else:
    instrument_list = src.InstrumentList()
    instrument_list.creation_date = cfg["time_string"]
    instrument_list.model_type = cfg["model_to_create"]
    instrument_list.read_instruments_from_instrument_dirs(cfg["dir_samples"])

cfg["instrument_list"] = instrument_list.instruments
cfg["num_instruments"] = instrument_list.get_n_instruments()

# Create empty dataset and figure list
data = src.SampleSet(cfg)
figures = list()

if cfg["generate_base_sample_batches"]:
    data.generate_base_sample_pickles(cfg["dir_samples"])

if cfg["generate_mix_sample_batches"]:
    data.mix_batchwise(cfg)

if cfg["load_cnn"]:
    if not os.path.isfile(cfg["model_to_load"]):
        raise RuntimeError("load_cnn is true but model file " + cfg["model_to_load"] + " does not exist.")

    # The compile=False statement is due to a Tensorflow 2.2.0 vs. 2.3.0 incompatibility
    model = load_model(cfg["model_to_load"], compile=False)
    print("{} loaded.".format(cfg["model_to_load"]))

    _, keras_callbacks = src.create_model_and_callbacks(cfg)

else:
    file_list = src.get_base_sample_batch_file_list(cfg)

    model, keras_callbacks = src.create_model_and_callbacks(cfg)
    model.summary()

if cfg["train_cnn"]:
    # Re-read file list after new data was generated
    file_list = src.get_base_sample_batch_file_list(cfg)

    model, keras_callbacks = src.train_model(cfg, file_list, model, data, keras_callbacks)
    src.save_model(model, cfg)
    cfg_file_name = "models/cfg_" + cfg["time_string"] + ".yaml"
    with open(cfg_file_name, "w") as f:
        yaml.dump(cfg, f)
    print("Saved config to %s." % cfg_file_name)

if cfg["save_instrument_list"]:
    instrument_list.save_instrument_list(cfg)

# Import and preprocess the prediction sound file
audio, audio_buffer, buffer_len = src.import_and_preprocess_sound_file(cfg)

# Calculate beats for beat-aligned analysis
beats, beats_t = src.get_tatum(audio, cfg, max_frame=buffer_len, plots=False)

# Predict data using beat-aligned frames
y_pred = src.predict_data(cfg, model, beats, buffer_len, audio_buffer, keras_callbacks)
print("Prediction max: " + str(np.max(y_pred)))

# y_pred = src.Sample.map_log_to_0to1_range(y_pred, cfg, 20)

# Patch the frames together
activations = src.average_output_matrix_frames(y_pred)
activations_pre = activations.copy()

# Calculate envelopes
ctrl_envelope, eval_envelope = src.calc_and_shift_input_envelope(
    cfg, audio, activations
)

# ctrl_envelope = src.Sample.map_log_to_0to1_range(ctrl_envelope, cfg, 20)

# Plot envelope comparison
if cfg["do_plot"]:
    figures.append(
        src.plot_envelope(
            ctrl_envelope,
            eval_envelope,
            ctrl_envelope - eval_envelope,
            audio.size / cfg["sr"],
            cfg,
            title="Envelopes uncorrected",
        )
    )


# Level the activations and envelopes globally using LMS
# eval_envelope, activations = src.level_amplitude_using_lms(
#     ctrl_envelope, eval_envelope, activations
# )

# Calculate and plot error envelope
error_envelope = ctrl_envelope - eval_envelope
if cfg["do_plot"]:
    figures.append(
        src.plot_envelope(
            ctrl_envelope,
            eval_envelope,
            error_envelope,
            audio.size / cfg["sr"],
            cfg,
            title="Envelopes corrected",
        )
    )

if cfg["refurbish_activations"]:
    # Enhance/refurbish the activations using a tanh-curve
    activations_refurbish = src.tanh_refurbish(
        activations, ctrl_envelope, eval_envelope, amp_factor=2
    )

    # Calculate the envelopes after refurbishing
    eval_envelope_post = np.sum(activations_refurbish, axis=0)
    error_envelope_post = ctrl_envelope - eval_envelope_post
    # Compare envelopes
    if cfg["do_plot"]:
        figures.append(
            src.plot_envelope_comparison(
                ctrl_envelope,
                eval_envelope,
                error_envelope,
                eval_envelope_post,
                error_envelope_post,
                audio.size / cfg["sr"],
                cfg,
                title=["Envelopes before refurbishment", "Envelopes after refurbishment"],
            )
        )
        figures.append(
            src.plot_activation_comparison(
                activations,
                activations_refurbish,
                cfg,
                audio.size / cfg["sr"],
                title=["Original activations", "Refurbished activations"],
            )
        )

if cfg["do_plot"]:
    figures.append(
        src.plot_activations(
            activations,
            cfg,
            audio.size / cfg["sr"],
            title="Activations",
        )
    )
    figures.append(
        src.plot_envelope(
            ctrl_envelope,
            eval_envelope,
            error_envelope,
            audio.size / cfg["sr"],
            cfg,
            title="Envelopes after LMS correction",
        )
    )
    # TODO implement additional plot function to avoid plotting equal activations twice
    figures.append(
        src.plot_activation_comparison(
            activations,
            activations,
            cfg,
            audio.size / cfg["sr"],
            title=["Original activations", "Refurbished activations"],
        )
    )

if cfg["save_activations_to_csv"]:
    src.save_results_to_csv(
        cfg, data, activations, ctrl_envelope, beats_t, cfg["csv_output_path"]
    )

    # Also save refurbished activations to CSV
    if cfg["refurbish_activations"]:
        src.save_results_to_csv(
            cfg,
            data,
            activations_refurbish,
            ctrl_envelope,
            beats_t,
            cfg["csv_output_path"],
            refurbished=True,
        )

if cfg["plot"]["save_images"]:
    for idx, fig in enumerate(figures):
        fig.write_image(os.path.join(cfg["csv_output_path"], "figure" + str(idx) + ".svg"))
