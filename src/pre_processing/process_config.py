import numpy as np
from os import scandir, path
from time import gmtime, strftime
import uuid


def process_config(cfg):
    """Processes config parameters in config.yaml.

    Args:
       cfg(Dict): config parameters, imported from config.yaml
    Raises:
    Returns:
       Processed config.
    """
    # Get cfg params
    n_bins_per_oct = cfg["n_bins_CQT"]
    n_octaves = cfg["n_octaves_CQT"]

    # parse directory names into instrument labels
    subfolders = [f.path for f in scandir(cfg["dir_samples"]) if f.is_dir()]
    subfolders = [f.split("/")[-1] for f in subfolders]
    [subfolders.remove(f) for f in subfolders if f[0] == "_"]
    cfg["instrument_list"] = subfolders

    # generate current tensorboard log path
    # we add a 2-digit UID in case 2 programs get started at exactly the right time
    time_string = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
    uid_str = str(uuid.uuid4())[:2]
    cfg["time_string"] = time_string + "_" + uid_str


    originalDirectory = cfg["tensorboard_log_path"]
    cfg["tensorboard_log_path"] = path.join(originalDirectory, cfg["time_string"])

    # Calc new cfg params
    cfg["CQT_divisor"] = [
        round(n_bins_per_oct[2] / n_bins_per_oct[0]),
        round(n_bins_per_oct[1] / n_bins_per_oct[0]),
        1,
    ]

    cfg["n_oct_total"] = np.sum(n_octaves)
    oct_range = np.cumsum(n_octaves)
    cfg["idx_b"] = [0, n_bins_per_oct[0] * oct_range[0]]
    cfg["idx_m"] = [n_bins_per_oct[1] * oct_range[0], n_bins_per_oct[1] * oct_range[1]]
    cfg["idx_h"] = [n_bins_per_oct[2] * oct_range[1], n_bins_per_oct[2] * oct_range[2]]
    cfg["n_bins_b"] = int(n_bins_per_oct[0] * cfg["n_oct_total"])
    cfg["n_bins_m"] = int(n_bins_per_oct[1] * cfg["n_oct_total"])
    cfg["n_bins_h"] = int(n_bins_per_oct[2] * cfg["n_oct_total"])
    cfg["oct_range"] = oct_range

    return cfg
