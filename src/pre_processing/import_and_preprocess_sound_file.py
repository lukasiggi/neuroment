import numpy as np
import librosa as lb


def import_and_preprocess_sound_file(cfg):
    # import soundfile
    audio_data, _ = lb.core.load(cfg["input_file_path"], sr=cfg["sr"], mono=True)
    # calculate buffer size
    n_samples_buffer = cfg["hop_length"] * (cfg["n_frames"] - 1)
    # buffering
    audio_buffer = lb.util.frame(
        audio_data, n_samples_buffer, hop_length=cfg["hop_length"]
    )

    buffer_len = int(audio_buffer.shape[1])

    return audio_data, audio_buffer, buffer_len
