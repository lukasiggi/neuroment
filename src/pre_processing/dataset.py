#!/usr/bin/env python
"""
Dataset.py

This File contains all necessary classes and functions to create a dataset from audio-samples.
The various read-in samples are modified and mixed before features are extracted.
The order of mixing and the various parameters can be set in the config file. See README for further info.

"""

import os
import time
import librosa as lb
import numpy as np
import matplotlib.pyplot as plt
import random
import pickle
from glob import iglob, glob
from pathlib import Path


class Sample:
    """
    The Sample object contains audio data and its corresponding features, labels and relations.

    Args:
        x_t (float): audio data(waveform)

    Attributes:
        X (list): list containing various feature-matrices, -vectors and/or -scalars.
        y (float): vector containing all possible instrument labels.
        x_t (float): audio data(waveform).
        id (dict): a dictionary containing all relevant info (filename, modifications, mixture, etc.).
        parents (list): references to all other Samples it was mixed from. If it wasn't mixed, the parents-list is empty.
        children (list): references to all other Samples it was mixed with.

    """

    # CONSTANTS
    # set min. file size to consider and exclude mac-OS .wav-files with size of ~4kb
    MIN_SIZE_SOUND_FILE = 4.5 * 1000  # ~4kB

    def __init__(self, x_t, id, cfg, file_name="", parents=[], children=[]):
        """Initializes a Sample object with the given parameters.
        Either the parameter file_name or y must be given.

         Args:
             x_t (np-array): waveform of sample
             id (dict): ID of sample
             cfg (list): Configuration parameters
             file_name (string or list of strings): file-name of sample file (optional)
                                                    if empty, the samples is a mix
             y_label (dict): label of sample, used in case of mixed/FXed sample (optional)
             y_value (array): np-array with 1 level per instrument
             parents (list): ids of parent samples
             children (list): ids of children samples
        """
        self.y = []
        self.x_t = x_t
        self.id = id
        self.parents = parents
        self.children = children

        if np.max(np.abs(self.x_t)) > 1.0:  # TODO move me
            self.x_t /= np.max(np.abs(self.x_t))

        self.X_bottom, self.X_middle, self.X_top = self.calc_stacked_cqt(self.x_t, cfg)
        self.generate_y_envelope(cfg)

        if id["inst"] == []:
            print("Error - empty instrument id: {}".format(id["filename"]))

    def set_parents(self, samples):
        """Returns the parent(s) of this Sample. A Parent is a Sample used in the mixture, which created this Sample

        Raises:
            RuntimeError: Sample has no parents.

        Returns:
            parents (list): A list containing ids of all parents.
        """
        self.parents = samples
        pass

    def set_children(self, samples):
        """Returns the children of this Sample. Children are Samples which are a result of a mixture with this Sample

        Raises:
            RuntimeError: Sample has no children.

        Returns:
            parents (list): A list containing ids of all children.
        """
        self.children = samples
        pass

    def get_parents(self):
        """Returns the parent(s) of this Sample. A Parent is a Sample used in the mixture, which created this Sample

        Raises:
            RuntimeError: Sample has no parents.

        Returns:

            parents (list): A list containing ids of all parents.
        """
        return self.parents

    def get_children(self):
        """Returns the children of this Sample. Children are Samples which are a result of a mixture with this Sample

        Raises:
            RuntimeError: Sample has no children.

        Returns:
            parents (list): A list containing ids of all children.
        """
        if not self.children:
            if __debug__:
                print("Sample has no children")
        return self.children

    def get_id(self):
        """Returns the id of of this Sample.

        Returns:
            id (dict): A dictionary containing all necessary information about this sample
        """
        return self.id

    def set_id(self, id):
        """Sets the id of of this Sample.

        Args:
            id (list): A dictionary containing all necessary information about this sample
        """
        self.id = id
        pass

    def get_inst(self):
        """Returns the instrument of of this Sample.

        Returns:
            inst (string): Name of the instrument
        """
        return self.id["inst"]

    def set_inst(self, inst):
        """Sets the instrument of of this Sample.

        Args:
            inst (string): Name of the instrument
        """
        self.inst = inst
        pass

    def get_y(self):
        return self.y

    def set_y(self, y):
        self.y = y
        pass

    def get_audio(self):
        """Returns the audio-data(waveform) of this Sample.

        Args:
            id (list): A dictionary containing all necessary information about this sample
        """
        return self.x_t

    @staticmethod
    def calc_envelope(x_t, cfg, X_bottom=None, X_middle=None, X_top=None):
        if cfg["envelope_type"] == "cqt":
            assert X_bottom is not None and X_middle is not None and X_top is not None

            # compute filter lengths of each CQT in the CQT stack
            cqt_lengths_b = lb.filters.constant_q_lengths(sr=cfg["sr"],
                                                          n_bins=cfg["n_bins_b"],
                                                          bins_per_octave=cfg["n_bins_CQT"][0],
                                                          fmin=cfg["f_min_CQT"],
                                                          window='hann',)
            cqt_lengths_m = lb.filters.constant_q_lengths(sr=cfg["sr"],
                                                          n_bins=cfg["n_bins_m"],
                                                          bins_per_octave=cfg["n_bins_CQT"][1],
                                                          fmin=cfg["f_min_CQT"],
                                                          window='hann',)
            cqt_lengths_h = lb.filters.constant_q_lengths(sr=cfg["sr"],
                                                          n_bins=cfg["n_bins_h"],
                                                          bins_per_octave=cfg["n_bins_CQT"][2],
                                                          fmin=cfg["f_min_CQT"],
                                                          window='hann',)

            # keep only filter lengths that are appearant in stacked CQT
            cqt_lengths_b = cqt_lengths_b[cfg["idx_b"][0] : cfg["idx_b"][1]]
            cqt_lengths_m = cqt_lengths_m[cfg["idx_m"][0] : cfg["idx_m"][1]]
            cqt_lengths_h = cqt_lengths_h[cfg["idx_h"][0] : cfg["idx_h"][1]]

            # stack bass, mid and high CQT filter lengths to vectors
            cqt_lengths_stacked = np.concatenate((cqt_lengths_b, cqt_lengths_m, cqt_lengths_h), axis=0)

            # stack the CQT
            cqt_stacked = np.concatenate((X_bottom, X_middle, X_top), axis=0)

            # take CQT, divide each bin by its respective filter length, square, sum up
            envelope = np.sum(((cqt_stacked / cqt_lengths_stacked[:, None]) ** 2.0), axis=0)

            # apply a correction factor to the CQT envelope (empirically calculated)
            envelope *= cfg["cqt_envelope_correction_factor"]

            return envelope

        elif cfg["envelope_type"] == "rms":
            # lb.cqt() automatically centers, so we need to center too
            envelope = lb.feature.rms(x_t, frame_length=cfg["N_fft"], hop_length=cfg["hop_length"],
                                      center=True)
            envelope = envelope[0, :]

            # This condition should ONLY be true if center=False
            if len(envelope) < cfg["n_frames"]:
                envelope = np.concatenate([envelope, np.zeros((cfg["n_frames"] - len(envelope)))])

            if cfg["debug"]:
                print("rms envelope min: " + str(np.min(envelope)) + ", max: " + str(np.max(envelope)))

            return envelope

        elif cfg["envelope_type"] == "stft":
            window = np.hanning(cfg["N_fft"])
            stft = lb.core.stft(
                x_t, n_fft=cfg["N_fft"],
                hop_length=cfg["hop_length"],
                win_length=cfg["N_fft"],
                window=window,
                center=True,
                pad_mode="reflect"
            )

            stft = 2 * np.abs(stft) / np.sum(window)
            envelope = np.sum(stft, axis=0)

            if cfg["norm_cqt_and_envelope"]:
                envelope /= cfg["N_fft"]

            if cfg["debug"]:
                print("stft envelope min: " + str(np.min(envelope)) + ", max: " + str(np.max(envelope)))

            return envelope

        elif cfg["envelope_type"] == "absolute":
            # Calc envelope using STFT
            fft_data = lb.core.stft(
                x_t, n_fft=cfg["N_fft"], hop_length=cfg["hop_length"],
            )
            absolute_spectrum = np.abs(fft_data)

            if cfg["norm_cqt_and_envelope"]:
                absolute_spectrum /= cfg["N_fft"]

            envelope = np.mean(absolute_spectrum, axis=0)
            return envelope

        else:
            raise ValueError(
                "Value "
                + cfg["envelope_type"]
                + " of config option 'envelope_type' not recognized."
            )

    def generate_y_envelope(self, cfg):
        """Generate label values for Y.
        Either file_name of y must be passed.

         Args:
             cfg (list): Configuration parameters
             y_value (numeric): values used for initialization of y
             file_name (string or list of strings): file-name(s) of sample file(s) (optional)
             y_label (dict): labels of original sample, used in case of mixed/FXed sample (optional)
        """

        if len(self.id["inst"]) == 1:
            envelope = self.calc_envelope(self.x_t, cfg, self.X_bottom, self.X_middle, self.X_top)

            y_matrix = np.zeros((cfg["num_instruments"], len(envelope)))
            y_matrix[self.id["inst"]] = envelope
            self.y = y_matrix
        else:
            self.y = None
        pass

    @staticmethod
    def calc_stacked_cqt(x_t, cfg):
        """Calculate stacked CQT by calculation of 3 different CQTs (bass,mids,highs) and stacking them.
        Function is marked static to be callable in main without creating an instance of Sample.
        """

        cqt_b = lb.cqt(
            x_t,
            hop_length=cfg["hop_length"],
            n_bins=cfg["n_bins_b"],
            bins_per_octave=cfg["n_bins_CQT"][0],
            fmin=cfg["f_min_CQT"],
            window='hann',
            scale=False,
        )
        cqt_b = np.abs(cqt_b)

        cqt_m = lb.cqt(
            x_t,
            hop_length=cfg["hop_length"],
            n_bins=cfg["n_bins_m"],
            bins_per_octave=cfg["n_bins_CQT"][1],
            fmin=cfg["f_min_CQT"],
            window='hann',
            scale=False,
        )
        cqt_m = np.abs(cqt_m)

        cqt_h = lb.cqt(
            x_t,
            hop_length=cfg["hop_length"],
            n_bins=cfg["n_bins_h"],
            bins_per_octave=cfg["n_bins_CQT"][2],
            fmin=cfg["f_min_CQT"],
            window='hann',
            scale=False,
        )
        cqt_h = np.abs(cqt_h)

        return (
            cqt_b[cfg["idx_b"][0] : cfg["idx_b"][1], :],
            cqt_m[cfg["idx_m"][0] : cfg["idx_m"][1], :],
            cqt_h[cfg["idx_h"][0] : cfg["idx_h"][1], :],
        )

    @staticmethod
    def map_0to1_to_log_range(data, cfg, log_factor):
        data = log_factor * np.log10(data)
        data = np.clip(data, cfg["min_energy_in_db"], 0) - cfg["min_energy_in_db"]
        return data

    @staticmethod
    def map_log_to_0to1_range(data, cfg, log_factor):
        data += cfg["min_energy_in_db"]
        data = np.power(10.0, data / log_factor)
        return data

class SampleSet:
    """
    The SampleSet object contains many Samples and provides functions for visualization and sonification.

    Args:
        parameters (list): the parameters for framing, re-sampling, feature-extraction etc.

    Raises:
        ValueError: value of config variable sample_naming not known

    Attributes:
        Samples (list): list of all samples.
        cfg (list): configuration parameters framing, re-sampling, feature-extraction etc.
        family_tree (tree): tree structure for the mixtures of all sounds.
        instrument_dict (dict): dictionary for all labels (to be yable to use keywords such as "kick").

    """

    # CONSTANTS
    # minimum level that max(x_t_mix) may have to normalize it
    MIN_LEVEL_MIX = 0.001

    def __init__(self, cfg):
        self.Samples = []
        self.Samples_mix = []
        self.cfg = cfg
        self.family_tree = []
        self.instrument_list = cfg["instrument_list"]
        self.num_instruments = cfg["num_instruments"]
        self.instrument_dict = dict(
            zip(self.instrument_list, range(self.num_instruments))
        )

    def generate_base_sample_pickles(self, path, plot_hist=False, file_name=[]):
        """Loads the sample data from a directory

        Args:
            path (char): the path of the sample directory

        Raises:
            RuntimeError: Couldn't load data. No path
            RuntimeError: List of samples failed to load
        """
        cfg = self.cfg
        n_samples_fr = cfg["hop_length"] * (cfg["n_frames"] - 1)

        # counter for the id
        usn_counter = 0
        lost_samples = 0
        output_ctr = 0

        file_list = [
            f for f in iglob(path + "/**", recursive=True) if os.path.isfile(f)
        ]
        np.random.shuffle(file_list)
        n_files = len(file_list)
        file_ctr = 0

        print("data generation started.")
        t_0 = time.time()
        print("wav-files are being read in.")
        # TODO check 1st if all sound-files in directory match an instrument in instrument-list
        for cur_file in file_list:
            cur_file = str(Path(cur_file))
            sound_file = cur_file
            if file_name:
                is_valid_sound_file = (
                    os.path.getsize(sound_file) > Sample.MIN_SIZE_SOUND_FILE
                ) and (sound_file.endswith(file_name))
            else:
                is_valid_sound_file = (
                    sound_file.endswith(".WAV") or sound_file.endswith(".wav")
                ) and (os.path.getsize(sound_file) > Sample.MIN_SIZE_SOUND_FILE)
            if is_valid_sound_file:
                file_ctr += 1
                wav_orig = lb.load(sound_file, mono=True, sr=cfg["sr"])
                # TODO check if wav is 1x1 tuple or numerical array already
                wav_orig, _ = lb.effects.trim(
                    np.array(wav_orig[0]), frame_length=cfg["N_fft"]
                )
                # shift data if specified
                for f in range(cfg["shift"]["shift_factor"] + 1):
                    wav = wav_orig
                    if f != 0:
                        wav = self.shift_audio(
                            wav,
                            np.random.randint(
                                cfg["shift"]["min_shift"], cfg["shift"]["max_shift"]
                            ),
                        )
                    if len(wav) / n_samples_fr > 1:
                        residual = wav
                        while len(residual) > n_samples_fr:
                            wav_frame = residual[0:n_samples_fr]

                            id, is_valid = self.generate_id(
                                usn_counter, sample_path=cur_file.format()
                            )
                            if is_valid:
                                self.Samples.append(
                                    Sample(wav_frame, id, cfg, file_name=cur_file)
                                )
                                usn_counter += 1

                            residual = residual[n_samples_fr:]

                            # File saving
                            if (len(self.Samples) > 0) and (len(self.Samples) % cfg["n_samples_per_batch"] == 0):

                                output_ctr += 1
                                print("saving {}. batch...".format(output_ctr))
                                output_samples = self.Samples[
                                    -cfg["n_samples_per_batch"] :
                                ].copy()
                                self.save_samples(
                                    output_samples, "base_{}".format(output_ctr)
                                )
                                self.Samples = []
                    else:
                        lost_samples += 1

                print(
                    "Loaded: "
                    + str(np.round(file_ctr / n_files * 100, 2))
                    + "% of data."
                )

        # Save the residual
        if (len(self.Samples) % cfg["n_samples_per_batch"]) != 0:
            output_ctr += 1
            print(
                "saving last batch... size: {}".format(
                    (len(self.Samples) % cfg["n_samples_per_batch"])
                )
            )
            output_samples = self.Samples[
                -(len(self.Samples) % cfg["n_samples_per_batch"]) :
            ].copy()
            self.save_samples(output_samples, "base_{}".format(output_ctr))
            self.Samples = []

        print("base samples loaded and batched.\n")

    def shift_audio(self, x, num_samples):
        """shifts the audio data belonging to a Sample object by a given amount.

        Args:
            num_samples (int): number of samples (amount of shifting, must be positive).
            sample (Sample): a Sample object containing audio data (x_t).

        Raises:
            RuntimeError: num_samples has an invalid value.

        Returns:
            S_shifted (Sample): new Sample object containing the shifted audio data(x_t).
        """

        # shift the signal by num_samples to the right
        x = np.roll(x, num_samples)
        # pad with zeros
        x[0:num_samples] = 0
        return x

    def create_mixes(self, cfg, spls=[], ctr_mix=None):
        """Choose samples and mix them.

        Levels are sampled from a normal distribution and limited to a min- and max-threshold.
        For each mix, first find the corresponding samples to mix, then calculate the mixing levels, then call
        mix_sample() to mix them and create a new object.

        Args:
            cfg (list): Configuration parameters
        """

        # Create list with number of samples to mix per mix
        n_samples_per_mix = np.random.normal(cfg["mix"]["mean_n_instruments"],
                                             cfg["mix"]["var_n_instruments"],
                                             cfg["n_samples_per_batch"])
        n_samples_per_mix = np.round(n_samples_per_mix)
        n_samples_per_mix = n_samples_per_mix.astype(int)
        n_samples_per_mix = np.clip(n_samples_per_mix, cfg["mix"]["min_n_instruments"],
                                    cfg["mix"]["max_n_instruments"])

        # Create list with
        # 1st column... true sample indices
        # 2nd column... CORRESPONDING scrambled sample indices
        n_unmixed_spl = len(spls)
        idx_rand = list(range(n_unmixed_spl))
        random.shuffle(idx_rand)

        mix_depth = 1
        ctr_samples = 0
        start_again = False
        if ctr_mix:
            output_ctr = ctr_mix
        else:
            output_ctr = 0

        while ctr_samples < cfg["n_samples_per_batch"]:

            # print("Mixed: " + str(np.round(ctr_samples / n_mixes * 100, 2)) + "% of data.")

            start_again = (
                start_again
                or (not idx_rand)
                or (len(idx_rand) < n_samples_per_mix[ctr_samples])
            )

            # If start_again is True, create new list of random indices
            if start_again:
                mix_depth += 1
                print("Reached mix_depth " + str(mix_depth))
                idx_rand = list(range(n_unmixed_spl))
                random.shuffle(idx_rand)
                start_again = False

            # Get indices of samples to mix
            spl_to_mix = list([0])  # init
            for it_mix in range(1, n_samples_per_mix[ctr_samples]):
                current_spl = it_mix
                while current_spl < len(idx_rand):
                    # Check if current instrument differs from all previous ones
                    is_diff_inst = True
                    # run through all samples found until now to check whether instruments differ or not
                    for it_sample in range(it_mix - 1, -1, -1):
                        different_sample = (
                            spls[idx_rand[current_spl]].get_inst()
                            != spls[idx_rand[it_sample]].get_inst()
                        )
                        is_diff_inst = is_diff_inst and different_sample

                    # If instrument is different, append to list. If not, continue search
                    if is_diff_inst:
                        spl_to_mix.append(current_spl)
                        break  # found sample
                    else:
                        current_spl += 1

            # Check if enough samples were found. Start with new random indices if not
            if len(spl_to_mix) < n_samples_per_mix[ctr_samples]:
                start_again = True

            # Get samples to mix
            sample_list = []
            [sample_list.append(spls[idx_rand[it_spl]]) for it_spl in spl_to_mix]

            # Create levels
            levels = np.random.normal(cfg["mix"]["mean_volume"], cfg["mix"]["var_volume"], len(spl_to_mix))

            # mix samples. check first if every sample has a valid id
            instr_nums = [s.get_id()["inst"] for s in sample_list]
            if all([isinstance(s[0], (int, float)) for s in instr_nums]):
                self.mix_sample(sample_list, levels, normalize_levels=False)
            else:
                print("Error: sample for mixing had empty ID")

            # File output
            if len(self.Samples_mix) % cfg["n_samples_per_batch"] == 0:
                if not ctr_mix:
                    output_ctr += 1
                output_samples = self.Samples_mix[-cfg["n_samples_per_batch"] :].copy()
                self.save_samples(output_samples, "mix_{}".format(output_ctr))
                self.Samples_mix = []

            # Remove indices of mixed samples from random index array
            # Consider the list becoming shorter with every del command
            [idx_rand.pop(it - count) for count, it in enumerate(spl_to_mix)]

            ctr_samples += 1

        # File output of the residual
        if (len(self.Samples_mix) % cfg["n_samples_per_batch"]) != 0:
            if not ctr_mix:
                output_ctr += 1
            output_samples = self.Samples_mix[
                -(len(self.Samples_mix) % cfg["n_samples_per_batch"]) :
            ].copy()
            self.save_samples(output_samples, "mix_{}".format(output_ctr))
            self.Samples_mix = []

        pass

    def mix_sample(self, samples, levels=[], normalize_levels=True):
        # TODO label the output Sample weighted with the levels?
        """Create a mixture of 2 or more samples.

        Args:
           samples (list): a list of the Sample objects containing the audio data to be mixed.
           levels (list): a list of the corresponding levels(float) for the mixture of the given samples.
           normalize_levels (bool): normalize the output if audio is clipping.

        Raises:
            RuntimeWarning: Audio is clipping. Set limiter=True when using levels which add up to a value
                            higher than 1

        Returns:
            S_mixed (Sample): new Sample object containing the mixed audio data(x_t).
        """

        if not len(levels):
            # mix all signals together equally
            levels = np.zeros(len(samples))
            for i in range(len(samples)):
                levels[i] = 1 / len(samples)  # not np.floor!
        else:
            # Normalize levels if necessary
            level_sum = sum(levels)
            if level_sum > 1:
                levels = levels / level_sum

        if len(levels) != len(samples):
            print("Error: the number of samples and levels doesn't match")
        else:
            x_t_mix = levels[0] * samples[0].get_audio()
            for spl, lvl in zip(samples[1:], levels[1:]):
                x_t_mix += lvl * spl.get_audio()
            try:
                # normalize only if x_t_mix is non-zero
                if normalize_levels and (np.max(x_t_mix) > SampleSet.MIN_LEVEL_MIX):
                    x_t_mix = x_t_mix / max(np.abs(x_t_mix))
            except RuntimeWarning:
                print("Got runtime warning with levels.")
                print("max(x_t_mix): " + str(max(x_t_mix)))

            # get the universal serial number for the id
            usn = len(self.Samples_mix)

            inst = []
            # TODO fix error - y is None, so y.shape fails
            y_mix = np.zeros(samples[0].y.shape)
            for ind, s in enumerate(samples):
                inst.append(s.id["inst"][0])
                y_mix += np.abs(levels[ind] * s.get_y())
            if inst is None:
                print("no instrument available")
            else:
                id, is_valid = self.generate_id(usn, mix=True, inst=inst)
                # Get parents IDs
                parentIDs = []
                [parentIDs.append(s.id) for s in samples]

                # check if mix signal is valid. If yes, generate new sample
                if all(np.isfinite(x_t_mix)):
                    new_sample = Sample(x_t_mix, id, self.cfg, parents=parentIDs)
                    new_sample.set_y(y_mix)
                else:
                    raise ValueError("non-finite sample in x_t_mix")

                # validate generated sample
                if self.validate_samples([new_sample]) and is_valid:
                    self.Samples_mix.append(new_sample)
                    [s.set_children(new_sample.id) for s in samples]

        pass

    def get_samples(
        self,
        usn=[],
        instruments=[],
        mixture=False,
        num_samples=1000,
        random=True,
        random_seed=0,
    ):

        all_usn = list(range(len(self.Samples)))
        picked_samples = np.zeros(len(self.Samples)).astype(bool)

        if usn:
            picked_samples[np.argwhere(np.isin(all_usn, usn) == True)[0]] = True

        if instruments:
            eps = np.finfo(float).eps
            for ind, s in enumerate(self.Samples):
                for instr in instruments:
                    if s.y[self.instrument_dict[instr]] > eps:
                        picked_samples[ind] = True
                        break

        if mixture:
            for ind, s in enumerate(self.Samples):
                if s.id["mix"]:
                    picked_samples[ind] = True

        temp = np.array(self.Samples)

        return list(temp[np.where(picked_samples == True)])

    def get_sample_data(self, samples=[]):
        """Returns the X and y data-samples of a list or all samples in the sampleset.

        Args:
            samples (list): list containing Sample-objects
        Raises:
            #TODO think of possible errors
        Returns:
            X_data (list): features of the chosen samples
            y_data (list): labels of the chosen samples
            ids (list): ids of the chosen Samples.
        """
        if not samples:
            samples = self.Samples
        X_bottom = []
        X_middle = []
        X_top = []
        y_data = []
        ids = []

        for s in samples:
            X_bottom.append(np.expand_dims(s.X_bottom, axis=-1))
            X_middle.append(np.expand_dims(s.X_middle, axis=-1))
            X_top.append(np.expand_dims(s.X_top, axis=-1))
            y_data.append(s.y)
            ids.append(s.get_id())

        X_bottom = np.array(X_bottom)
        X_middle = np.array(X_middle)
        X_top = np.array(X_top)
        y_data = np.array(y_data)

        # X_data: 3-dimensional tuple with samples x features x frames
        # y_data: 2-dimensional tuple with samples x instruments
        return X_bottom, X_middle, X_top, y_data, ids

    def generate_id(self, usn, sample_path="", mix=False, inst=None):
        """generates a universal id for a given sample

        Args:
            usn (int): universal serial number
            name (str, optional): name of the sample. Defaults to "".
            mix (bool, optional): If true the sample is a mixture of other samples. Defaults to False.
            inst ([type], optional): The instruments presents in the sample. Defaults to None.

        Returns:
            [type]: [description]
        """

        is_valid = True

        if inst is None:
            # sample is new, no instrument assigned yet
            inst = []

            # get instrument name
            instrument_name = ""

            if sample_path and (os.path.sep in sample_path):
                path_parts = sample_path.split(os.path.sep)
                sample_dir, file_name = path_parts[len(path_parts) - 2 :]

                # directories starting with "_" are not considered
                if sample_dir[0] != "_":
                    instrument_name = sample_dir
                else:
                    is_valid = False
            else:
                print("Error: Sample name is empty.")

            # get and appendinstrument number
            for i, ind in zip(self.instrument_list, range(self.num_instruments)):
                if instrument_name == i:
                    inst.append(ind)
                    break
            if inst is None:
                print("Error: no instrument detected")
                is_valid = False

        id = {"usn": usn, "mix": mix, "filename": sample_path, "inst": inst}
        return id, is_valid

    # TODO change argument to sample
    def plot_samples(self, samples):
        """plots a given sample

        Args:
            samples (Sample): sample to be plotted
        """
        # TODO put me into a seperate file

        dim = int(np.ceil(np.sqrt(len(samples))))

        fig, axs = plt.subplots(dim, dim, figsize=(9, 9), facecolor="w", edgecolor="k")
        fig.subplots_adjust(hspace=0.5, wspace=0.001)
        plt.title("Samples")

        axs = axs.ravel()

        for s, i in zip(samples, range(len(samples))):
            axs[i].matshow(s.X, origin="lower")
            axs[i].set_aspect("auto")

        plt.show()

    pass

    def validate_samples(self, samples=[]):
        """Validates samples using Librosas valid_audio() function and removes invalid samples.
        Depending on whether samples are passed either these samples or all samples in self.Samples are validated.

             Args:

             Raises:
                 #TODO Think of possible errors.
        """
        ctr_invalid = 0
        idx_invalid = []

        # Check if samples where passed or if all samples in self.Samples should be validated
        if not len(samples):
            samples = self.Samples

        # Validate samples
        for it, s in enumerate(samples):
            if (
                (not lb.util.valid_audio(s.x_t))
                or (s.get_y is None)
                or (s.id["inst"] is None)
            ):
                idx_invalid.append(it)
                ctr_invalid += 1

        # Remove invalid samples
        if ctr_invalid:
            print("Removing " + str(ctr_invalid) + " invalid samples .")
            ctr_remove = 0
            for it in idx_invalid:
                samples.pop(idx_invalid[it] - ctr_invalid)

        return not ctr_invalid  # return if valid or not

    def mix_batchwise(self, cfg):
        print("Started mixing...")
        file_list = glob(str(Path(cfg["dir_dataset"]) / "base*.pkl"))
        n_files = float(len(file_list))
        ctr_mix = 1
        for idx, file in enumerate(file_list):
            with open(file, "rb") as f:
                samples = pickle.load(f)
                self.create_mixes(cfg, samples, ctr_mix=ctr_mix)
                print("saving {}. mix-batch...".format(ctr_mix))
                ctr_mix += 1
                del samples
            print(f"{(idx+1)*100/n_files:.2f}" + "% of data mixed.")

        print("created and saved mixes.\n")

    def save_samples(self, obj, filename):
        # Create directory if it does not exist yet
        if not os.path.exists(self.cfg["dir_dataset"]):
            os.makedirs(self.cfg["dir_dataset"])
        with open(self.cfg["dir_dataset"] + filename + ".pkl", "wb") as output:
            pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)
        pass

    def merge_load(self, samples, cfg):
        pass


__author__ = "Lorenz Häusler, Lukas Maier"
__copyright__ = "Copyright 2019, IEM (Graz)"
__credits__ = "Lorenz Häusler", "Lukas Maier", "Alois Sontacchi"
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Lorenz Häusler, Lukas Maier"
__email__ = "Haeusler.Lorenz@gmail.com", "l.maier@student.tugraz.at"
__status__ = "Production"
