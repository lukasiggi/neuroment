def update_instruments_in_config(cfg, model):
    """
    Derives instrument_list and num_instruments from model.name and saves it to config.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file
        model (Keras functional): Keras CNN model
        activations (np.ndarray): evaluated activation function per instrument

    Returns:
        cfg (dict): configuration parameters with updated instrument_list and num_instruments

    """

    # model name can be read-only
    if "." not in model.name:
        print("Warning: model name is non-default")

    cfg["instrument_list"] = model.name.split(".")
    cfg["num_instruments"] = len(cfg["instrument_list"])

    return cfg
