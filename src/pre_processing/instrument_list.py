#!/usr/bin/env python
import yaml
import time
from os import scandir
import numpy as np


class InstrumentList:
    def __init__(self, *args):
        if len(args) == 0:
            # Initializes an empty InstrumentList.
            self.instruments = []
            self.model_type = ""
            self.creation_date = None

        elif len(args) == 1:
            # arg1: instrument_list_file_path
            # Initializes an InstrumentList object from a file with a previously used instrument list.
            instrument_dict = self.read_instrument_list_from_file(args[0])

            self.instruments = instrument_dict["instruments"]
            self.model_type = instrument_dict["model_type"]
            self.creation_date = None

        elif len(args) == 2:
            # arg1: instruments (list of str)
            # arg2: model_type (str)
            # Initializes an empty from a given list of instruments and model_type.
            self.instruments = args[0]
            self.model_type = args[1]
            self.creation_date = None

    def read_instrument_list_from_file(self, instrument_list_file_path):
        with open(instrument_list_file_path, "r") as instrument_list_file:
            instrument_list = yaml.load(instrument_list_file, Loader=yaml.FullLoader)

        return instrument_list

    def read_instruments_from_instrument_dirs(self, dir_with_instrument_folders):
        subfolders = [
            f.path for f in scandir(dir_with_instrument_folders) if f.is_dir()
        ]
        subfolders = [f.split("/")[-1] for f in subfolders]

        # Don't use directories with "_" prefix
        delete_idx = []
        [
            delete_idx.append(idx)
            for (idx, subfolder) in enumerate(subfolders)
            if subfolder[0] == "_"
        ]

        self.instruments = np.delete(subfolders, delete_idx)

    def save_instrument_list(self, cfg):
        with open("models/instrument_list_" + cfg["time_string"] + ".yaml", "w") as instrument_list_file:
            yaml.dump(
                {
                    "instruments": self.instruments.tolist(),
                    "model_type": self.model_type,
                    "creation_date": self.creation_date,
                },
                stream=instrument_list_file,
            )

    def get_n_instruments(self):
        return len(self.instruments)
