import numpy as np
import librosa as lb
import matplotlib.pyplot as plt
from scipy.signal import stft


def adaptive_whitening(X_stft, r=0.6, mu=0.997):
    """Applies adaptive whiteing to spectrogram data

    Args:
        X_stft (np.ndarray): spectrogram data
        r (float, optional): Defaults to 0.6.
        mu (float, optional): Defaults to 0.997.

    Returns:
        np.ndarray: whitened spectrogram data
    """
    P = np.zeros(X_stft.shape)
    P[:, 0] = np.maximum(X_stft[:, 0], r)
    for i in range(1, X_stft.shape[1]):
        P[:, i] = np.maximum(np.abs(np.maximum(X_stft[:, i], r)), mu * P[:, i - 1])
    return X_stft / P


def onset_detection(X):
    """calculates a simple onset detection

    Args:
        X ([type]): [description]

    Returns:
        [type]: [description]
    """
    delta = np.zeros(np.shape(X))
    delta[:, 1:] = np.diff(X, axis=1)
    delta[np.where(delta < 0)] = 0
    OSD = np.sum(delta, axis=0)
    return OSD / np.max(OSD)


def calculate_tempogram(OSD, cfg, tat_min=0.06, tat_max=0.428, n_tat=240, L=150):
    """calculates the tempogram for a given onset detection

    Args:
        OSD (np.ndarray): Onset detection
        cfg (dict): config parameter derrived from a YAML configuration file.
        tat_min (float, optional): lower boundry for the evaluated tatum. Defaults to 0.06.
        tat_max (float, optional): upper boundry for the evaluated tatum. Defaults to 0.428.
        n_tat (int, optional): number of tatums to be evaluated. Defaults to 240.
        L (int, optional): Length of the tempogram. Defaults to 150.

    Returns:
        [type]: tempogram
    """

    N = len(OSD)  # Number of blocks in total
    w = np.hanning(L)
    J = N - L + 1
    ODF_blocked = np.empty((L, J))
    sr_ODF = cfg["sr"] / cfg["hop_length"]

    for j in range(0, J):
        ODF_blocked[:, j] = OSD[j : j + L] * w  # Slice ODF in blocks and apply window

    # Create a transformation matrix to avoid FOR−loops
    tau = np.logspace(np.log10(tat_max), np.log10(tat_min), n_tat)
    n = np.outer(((-2j * np.pi / tau) / sr_ODF), np.linspace(0, L - 1, L))
    kern = np.exp(n)

    M = kern.dot(ODF_blocked)

    return M, tau, J, L


def tempopath(M, cfg, tau, theta=20, kappa=100):
    """evaulates the optimal tempo path within the tempogram

    Args:
        M (np.ndarray): Tempogram
        cfg (dict): config parameter derrived from a YAML configuration file.
        tau (list, np.ndarray): vector containing the possible tau values
        theta (int, optional): Defaults to 20.
        kappa (int, optional): Defaults to 100.

    Returns:
        np.ndarray: optimal tempo path values
    """

    n = M.shape[0]
    m = M.shape[1]

    sr_ODF = cfg["sr"] / cfg["hop_length"]

    dPhi = np.zeros(np.shape(M))
    dPhi[:, 1:] = np.diff(np.angle(M), axis=1)  # Actual phase difference
    dTildePhi = 2 * np.pi / sr_ODF / tau  # Expected phase difference
    deltaPhi = (dPhi.T - dTildePhi).T  # phase deviation
    deltaPhiMapped = np.mod(deltaPhi + np.pi, 2 * np.pi) - np.pi
    weight = np.power((1 - np.abs(deltaPhiMapped) / np.pi), kappa)

    M = np.multiply(M, weight)

    Mabs = np.abs(M)
    Mabs = Mabs / np.max(Mabs)

    D = np.empty(M.shape)
    D[:] = np.nan
    D[:, 0] = np.squeeze(Mabs[:, 0])
    idx = np.zeros((n, m - 1))

    for ii in range(1, m):
        for jj in range(0, n):
            tempoDiff = np.abs(1 / tau - 1 / tau[jj])
            tauDiff = np.abs(tau - tau[jj])
            mask = tauDiff < 0.03
            mask = mask.astype(int)
            mat = np.multiply(D[:, ii - 1] - theta * tempoDiff.T, mask)
            temp = np.max(mat)
            idx[jj, ii - 1] = np.argmax(mat)
            D[jj, ii] = Mabs[jj, ii] + temp

    tau_ind = np.zeros(m)
    tau_ind[m - 1] = np.argmax(D[:, -1])
    for ii in range(m - 2, -1, -1):
        tau_ind[ii] = idx[int(tau_ind[ii + 1]), ii]

    return tau_ind, D, Mabs, dTildePhi


def get_tatum(X, cfg, max_frame=None, plots=False):
    """Evaluates the tatum for all instances within a given spectrogram array

    Args:
        X ([type]): [description]
        cfg ([type]): [description]
        max_frame (int): maximum frame where a beat index may be
        plots (bool, optional): [description]. Defaults to False.

    Returns:
        [type]: [description]
    """
    X_stft = np.abs(
        lb.stft(y=X, n_fft=cfg["N_fft"], hop_length=cfg["N_fft"] - cfg["hop_length"])
    )

    n_frames = X_stft.shape[1]
    N = X_stft.shape[0]

    x_wh = adaptive_whitening(X_stft)

    mel_filters = lb.filters.mel(
        sr=cfg["sr"], n_fft=cfg["N_fft"], n_mels=50, fmin=94, fmax=15375
    )
    x_mel = np.dot(mel_filters, x_wh)
    # compression factor
    x_mel = np.log10(2 * x_mel + 1)

    OSD = onset_detection(x_mel)
    M, tau, J, L = calculate_tempogram(OSD, cfg)

    tau_ind, D, Mabs, d_phi = tempopath(M, cfg, tau)
    tau_ind = tau_ind.astype(int)
    # tau_opt = tau[tau_ind]

    #
    phi = np.zeros(J)
    for j in range(J):
        phi[j] = np.angle(M[tau_ind[j], j]) + 0.5 * L * d_phi[tau_ind[j]]

    crossings_pos = np.squeeze(np.array(np.where(np.diff(np.sign(np.sin(phi))) > 0)))
    crossings_start = int(np.mean(np.diff(crossings_pos[0:5])))
    crossings_end = int(np.mean(np.diff(crossings_pos[-7:-1])))

    beats_start = np.arange(
        0, int(L / 2) + crossings_pos[0] - 1 - crossings_start, crossings_start
    )
    beats_end = np.arange(
        int(L / 2) + crossings_pos[-1] - 2 + crossings_end, n_frames, crossings_end
    )
    beats = np.concatenate(
        (beats_start, (L / 2 + crossings_pos - 2), beats_end), axis=0
    )
    beats = beats.astype(int)

    t_frames = (N / 2 + np.arange(0, n_frames - 1, 1) * cfg["hop_length"]) / cfg["sr"]
    beats_t = t_frames[beats[:-1]]

    if plots:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.matshow(
            X_stft,
            origin="lower",
        )
        ax.set_aspect("auto")
        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.matshow(
            x_wh,
            origin="lower",
        )
        ax.set_aspect("auto")
        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.matshow(
            x_mel,
            origin="lower",
        )
        ax.set_aspect("auto")
        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(OSD)
        ax.set_aspect("auto")
        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.matshow(Mabs, origin="lower")
        ax.set_aspect("auto")
        plt.show()

    # Remove beat indices that lay behind end of audio sample
    beats = np.delete(beats, np.where(beats > max_frame))

    return beats, beats_t
