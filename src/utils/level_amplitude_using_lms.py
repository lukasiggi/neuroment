import numpy as np


def level_amplitude_using_lms(ctrl_envelope, eval_envelope, activations):
    """
    Calculates the actual and predicted envelopes for given audio_sample data and activations.

    Args:
        ctrl_envelope (np.ndarray): envelope of audio_sample
        eval_envelope (np.ndarray): envelope of activations
        activations (np.ndarray): evaluated activation function per instrument

    Returns:
        eval_envelope (np.ndarray): leveled envelope of activations
        activations (np.ndarray): leveled activation function per instrument
    """

    # define LMS parameters
    beta = np.linspace(0, 10, 10000)
    error = np.zeros(beta.size)

    # calc minimum possible error
    for ii, a in enumerate(beta):
        error[ii] = np.mean((ctrl_envelope - a * eval_envelope) ** 2)
    beta_opt = beta[np.argmin(error)]

    # apply factor to activations and envelope
    activations *= beta_opt
    eval_envelope *= beta_opt

    return eval_envelope, activations
