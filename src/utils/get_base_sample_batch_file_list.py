from os import listdir


def get_base_sample_batch_file_list(cfg):
    """
    Reads the base samples file list from hard drive according to config parameters.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file

    Returns:
        file_list (list): batch file list of base samples
    """

    file_list = listdir(cfg["dir_dataset"])

    try:
        file_list.remove(".DS_Store")
    except:
        print("Warning: removed .DS_Store from file_list(Mac users only).")

    if file_list[0] == ".gitkeep":
        file_list.pop(0)

    return file_list
