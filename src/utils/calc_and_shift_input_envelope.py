import numpy as np

from src.pre_processing.dataset import Sample


def calc_and_shift_input_envelope(cfg, audio_sample, activations):
    """
    Calculates the actual and predicted envelopes for given audio_sample data and activations.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file
        audio_sample (np.ndarray): audio data in time domain
        activations (np.ndarray): evaluated activation function per instrument

    Returns:
        ctrl_envelope (np.ndarray): envelope of audio_sample
        eval_envelope (np.ndarray): envelope of activations
    """

    # get the envelope of the input
    X_bottom, X_middle, X_top = Sample.calc_stacked_cqt(audio_sample, cfg)
    ctrl_envelope = Sample.calc_envelope(audio_sample, cfg, X_bottom, X_middle, X_top)

    # get the envelope of the evaluated activations
    eval_envelope = np.sum(activations, axis=0)

    # shift the input envelope to fit the evaluated envelope
    offset = np.argmax(np.correlate(ctrl_envelope, eval_envelope))
    ctrl_envelope = ctrl_envelope[offset:]
    ctrl_envelope = ctrl_envelope[0 : eval_envelope.size]

    return ctrl_envelope, eval_envelope
