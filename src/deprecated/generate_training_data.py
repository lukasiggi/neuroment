import numpy as np
import os
from pprint import pprint
import sys
from tensorflow.keras.models import load_model
import yaml
import librosa as lb
import matplotlib.pyplot as plt
import pickle

import src
from src.neural_network.model_data_loader import ModelDataGenerator
from src.neural_network.cnn_models import get_easy_cnn_structure, init_keras_callbacks


###### PARAMETERS ###########
N_SAMPLES_PER_PICKLE = 8
N_PICKLES_TO_GENERATE = 3
SEGMENT_LENGTH_IN_S = 0.3
DEBUG = False
#############################

# change working dir to dir of this file if necessary
if os.path.dirname(__file__) != "":
    os.chdir(os.path.dirname(__file__))

# check if there was a config file parsed on command line
if len(sys.argv) >= 2:
    config_file = sys.argv[1]
else:
    config_file = "config.yaml"

# get config parameters
with open(config_file, "r") as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    cfg = src.process_config(cfg)

try:
    print("Slurm job ID: %s" % int(os.environ["SLURM_JOB_ID"]))
    # deactivate plotting because we're on a headless system
    cfg["do_plot"] = False
    cfg["verbose"] = 2  # don't display progress bar
except:
    print("Did not detect slurm job ID.")
    cfg["verbose"] = 1  # display progress bar

# we print our cfg parameters s.t. we can easily look them up in the log later
pprint(cfg)

instrument_list = ["bass", "cymbals", "guitar", "hihat", "kick", "piano", "snare"]

cfg["batch_size"] = N_SAMPLES_PER_PICKLE
model_data_generator = ModelDataGenerator(cfg, instrument_list, N_PICKLES_TO_GENERATE, segment_length_in_s=SEGMENT_LENGTH_IN_S)

for (i_pickle, data) in enumerate(model_data_generator):
    features, labels = data

    file_name = cfg["dir_dataset"] + "train_" + str(i_pickle) + ".pkl"
    with open(cfg["dir_dataset"] + "train_" + str(i_pickle) + ".pkl", "wb") as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)

    print("Saved pickle %s." % file_name)
