"""
Neuroment - Instrument detection using Convolutional Neural Networks
---
To run the project simply run this main script. All configuration parameters for the project can be changed
in config.yaml. Packages can be installed using environment.yaml.

Authors:
- Lukas Maier, maier.lukas1995@gmail.com
- Lorenz Haeusler, haeusler.lorenz@gmail.com

This project is licensed under the GNU GPLv3 license.
See the LICENSE file or <https://www.gnu.org/licenses/> for the full license text.
"""

import numpy as np
import os
from pprint import pprint
import sys
from tensorflow.keras.models import load_model
import yaml
import librosa as lb
import matplotlib.pyplot as plt

import src

from src.neural_network.model_data_loader import ModelDataLoaderNew, ModelDataGenerator
from src.neural_network.cnn_models import get_easy_cnn_structure, init_keras_callbacks


###### PARAMETERS ###########
SEGMENT_LENGTH_IN_S = 0.3
N_STEPS_PER_EPOCH = 2
N_EPOCHS = 3
DEBUG = False
#############################

# change working dir to dir of this file if necessary
if os.path.dirname(__file__) != "":
    os.chdir(os.path.dirname(__file__))

# check if there was a config file parsed on command line
if len(sys.argv) >= 2:
    config_file = sys.argv[1]
else:
    config_file = "config.yaml"

# get config parameters
with open(config_file, "r") as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    cfg = src.process_config(cfg)

try:
    print("Slurm job ID: %s" % int(os.environ["SLURM_JOB_ID"]))
    # deactivate plotting because we're on a headless system
    cfg["do_plot"] = False
    cfg["verbose"] = 2  # don't display progress bar
except:
    print("Did not detect slurm job ID.")
    cfg["verbose"] = 1  # display progress bar

# we print our cfg parameters s.t. we can easily look them up in the log later
pprint(cfg)

instrument_list = ["bass", "cymbals", "guitar", "hihat", "kick", "piano", "snare"]

model_data_loader = ModelDataLoaderNew(cfg)
callbacks = init_keras_callbacks(cfg)

# we get the input shape from 1 feature vector
features, labels = model_data_loader.__getitem__(0)
input_shape = list(features.shape)
input_shape = input_shape[1:]  # let's keep batch size variable
input_shape = tuple(input_shape)

# somehow batch norm weights get nan ...
model = get_easy_cnn_structure(cfg, input_shape, len(instrument_list), features.shape[2], with_batch_norm=False)
model.summary()

if cfg["load_cnn"]:
    model.load_weights(cfg["model_to_load"])

if cfg["train_cnn"]:
    # TODO split into train val
    model.fit(
        model_data_loader,
        epochs=N_EPOCHS,
        callbacks=callbacks,
        batch_size=cfg["batch_size"],
        # validation_batch_size=cfg["batch_size"],
        verbose=cfg["verbose"],
    )
    model.save("models/easy_%s.h5" % cfg["time_string"])

audio, _ = lb.load(cfg["input_file_path"], sr=cfg["sr"], mono=True, offset=0.0)

model_data_loader.on_epoch_end()
features, labels = model_data_loader.__getitem__(0)
n_frames_per_feature = features.shape[2]

n_samples_per_data_point = int(SEGMENT_LENGTH_IN_S * cfg["sr"])

n_frames = len(audio) // n_samples_per_data_point + 1
n_zeros_to_pad = n_frames * n_samples_per_data_point - len(audio)
audio = np.concatenate((audio, np.zeros(n_zeros_to_pad)))

audio_buffer = audio.reshape((n_frames, -1)).T

predictions = np.zeros((n_frames, n_frames_per_feature, len(instrument_list)))
model_data_generator = ModelDataGenerator(cfg, instrument_list, 1, segment_length_in_s=SEGMENT_LENGTH_IN_S)

for frame in range(n_frames):
    features = model_data_generator._get_features_for_audio(audio_buffer[:, frame])
    if DEBUG:
        plt.plot(audio_buffer[:, frame])
        plt.show()
        plt.imshow(lb.amplitude_to_db(features), aspect="auto", origin="lower",)
        plt.show()

    features = features.reshape((1, features.shape[0], features.shape[1], 1))

    predictions_new = model.predict(
        features,
        batch_size=cfg["batch_size"],
        callbacks=callbacks,
    )
    predictions[frame, :, :] = predictions_new

predictions_reshape = predictions.reshape((predictions.shape[0] * predictions.shape[1], predictions.shape[2]))

src.plot_activations(
    predictions_reshape,
    cfg,
    audio.size / cfg["sr"],
    title="Activations",
)

if cfg["verbose"] == 1:
    # not on server
    plt.plot(predictions_reshape)
    plt.legend(instrument_list)
    plt.show()
