import numpy as np
import os
import yaml
import librosa as lb
import matplotlib.pyplot as plt

import src


def hl_envelopes_idx(s, dmin=1, dmax=1, split=False):
    """
    Input :
    s: 1d-array, data signal from which to extract high and low envelopes
    dmin, dmax: int, optional, size of chunks, use this if the size of the input signal is too big
    split: bool, optional, if True, split the signal in half along its mean, might help to generate the envelope in some cases
    Output :
    lmin,lmax : high/low envelope idx of input signal s
    """

    # locals min
    lmin = (np.diff(np.sign(np.diff(s))) > 0).nonzero()[0] + 1
    # locals max
    lmax = (np.diff(np.sign(np.diff(s))) < 0).nonzero()[0] + 1

    if split:
        # s_mid is zero if s centered around x-axis or more generally mean of signal
        s_mid = np.mean(s)
        # pre-sorting of locals min based on relative position with respect to s_mid
        lmin = lmin[s[lmin] < s_mid]
        # pre-sorting of local max based on relative position with respect to s_mid
        lmax = lmax[s[lmax] > s_mid]

    # global max of dmax-chunks of locals max
    lmin = lmin[
        [i + np.argmin(s[lmin[i : i + dmin]]) for i in range(0, len(lmin), dmin)]
    ]
    # global min of dmin-chunks of locals min
    lmax = lmax[
        [i + np.argmax(s[lmax[i : i + dmax]]) for i in range(0, len(lmax), dmax)]
    ]

    return lmin, lmax


os.chdir("../../")

SAMPLE_RATE = 48000

# Get config parameters
with open("config.yaml", "r") as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    cfg = src.process_config(cfg)

# Create empty dataset and figure list
data = src.SampleSet(cfg)

input_audio, _ = lb.load("data/input/rock_your_body.wav", SAMPLE_RATE, mono=True)
input_audio = input_audio[0:150000]
t_input = np.linspace(0.0, len(input_audio - 1) / SAMPLE_RATE, len(input_audio))

cqt_b, cqt_m, cqt_h = src.Sample.extract_features(input_audio, cfg)
envelope = src.Sample.calc_envelope(input_audio, cfg)
t_cqt = np.linspace(0.0, len(input_audio - 1) / SAMPLE_RATE, len(envelope))

# analytic_signal = hilbert(input_audio, N=1024)
# amplitude_envelope = np.abs(analytic_signal)
# t_envelope = np.linspace(0.0, len(input_audio - 1) / SAMPLE_RATE, len(amplitude_envelope))

fft_data = lb.core.stft(input_audio, n_fft=1024, hop_length=512)
fft_data = np.abs(fft_data)
fft_envelope = np.mean(fft_data, axis=0)
t_envelope = np.linspace(0.0, len(input_audio - 1) / SAMPLE_RATE, len(fft_envelope))

# envelope_smoothed, _ = hl_envelopes_idx(input_audio)
# t_envelope = np.linspace(0.0, len(input_audio - 1) / SAMPLE_RATE, len(envelope_smoothed))

fft_envelope /= max(fft_envelope)
envelope /= max(envelope)
plt.figure(figsize=(16, 9))
plt.plot(t_input, np.abs(input_audio))
plt.plot(t_cqt, envelope)
plt.plot(t_envelope, fft_envelope)
plt.show()
# plt.subplots(2, 1)
# plt.subplot(211)
# plt.plot(input_audio)
#
# plt.subplot(212)
# plt.plot(envelope)
# plt.show()