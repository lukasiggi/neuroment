import numpy as np
import os
import yaml
import librosa as lb
import matplotlib.pyplot as plt
import soundfile as sf

import src


os.chdir("../../")

##### PARAMETERS #############
SAMPLE_RATE = 48000
SAMPLES_TO_MIX = [
    "data/training/guitar/guitar_full.wav",
    "data/training/snare/snare_full.wav",
]
##############################


# Get config parameters
with open("config.yaml", "r") as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    cfg = src.process_config(cfg)

sample_set = src.SampleSet(cfg)
sample_list = []
sample_counter = 0

for sample_path in SAMPLES_TO_MIX:
    sample_id, is_valid = sample_set.generate_id(sample_counter, sample_path)
    sample_counter += 1
    assert is_valid

    input_audio, _ = lb.load(sample_path, SAMPLE_RATE, mono=True)
    sample_set.Samples.append(src.Sample(input_audio, sample_id, cfg))

sample_set.mix_sample(sample_set.Samples)

sf.write("sample_1.wav", sample_set.Samples[0].x_t, samplerate=SAMPLE_RATE)
sf.write("sample_2.wav", sample_set.Samples[1].x_t, samplerate=SAMPLE_RATE)
sf.write("mix.wav", sample_set.Samples_mix[0].x_t, samplerate=SAMPLE_RATE)
