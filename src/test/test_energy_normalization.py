import numpy as np
import os
import yaml
import librosa as lb
import matplotlib.pyplot as plt

import src

run_test = [0, 0, 0, 0, 0, 0, 0, 1]
test_signal = 0

def amplitude_to_db(value):
    return 20 * np.log10(value)


def compare_single_frame(cqt, cqt_compensated, stft, stft_compensated, frame, use_log):
    print_max = True
    if use_log:
        if print_max:
            print("Max comparison frame " + str(frame) + ": ")
            print("- cqt: " + str(amplitude_to_db(np.max(cqt[:, frame]))))
            print("- cqt compensated: " + str(amplitude_to_db(np.max(cqt_compensated[:, frame]))))
            print("- stft: " + str(amplitude_to_db(np.max(stft[:, frame]))))
            print("- stft compensated: " + str(amplitude_to_db(np.max(stft_compensated[:, frame]))))

        print("Mean comparison frame " + str(frame) + ": ")
        print("- cqt: " + str(amplitude_to_db(np.mean(cqt[:, frame]))))
        print("- cqt compensated: " + str(amplitude_to_db(np.mean(cqt_compensated[:, frame]))))
        print("- stft: " + str(amplitude_to_db(np.mean(stft[:, frame]))))
        print("- stft compensated: " + str(amplitude_to_db(np.mean(stft_compensated[:, frame]))))
    else:
        if print_max:
            print("Max comparison frame " + str(frame) + ": ")
            print("- cqt: " + str(np.max(cqt[:, frame])))
            print("- cqt compensated: " + str(np.max(cqt_compensated[:, frame])))
            print("- stft: " + str(np.max(stft[:, frame])))
            print("- stft compensated: " + str(np.max(stft_compensated[:, frame])))

        print("Mean comparison frame " + str(frame) + ": ")
        print("- cqt: " + str(np.mean(cqt[:, frame])))
        print("- cqt compensated: " + str(np.mean(cqt_compensated[:, frame])))
        print("- stft: " + str(np.mean(stft[:, frame])))
        print("- stft compensated: " + str(np.mean(stft_compensated[:, frame])))


os.chdir("../../")

SAMPLE_RATE = 48000

# Get config parameters
with open("config.yaml", "r") as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    cfg = src.process_config(cfg)

cfg["instrument_list"] = []
cfg["num_instruments"] = 0

# Create empty dataset and figure list
data = src.SampleSet(cfg)

if test_signal == 0:
    input_audio, _ = lb.load("data/input/rock_your_body.wav", SAMPLE_RATE, mono=True)
    input_audio = input_audio[0:150000]
elif test_signal == 1:
    f0 = 200
    input_audio = np.sin(np.linspace(0, 2, SAMPLE_RATE * 2) * (2 * np.pi * f0))

t_input = np.linspace(0.0, len(input_audio - 1) / SAMPLE_RATE, len(input_audio))

############# TEST 1 ######################################################
# Compare single CQT amplitude with STFT amplitude
if run_test[0]:
    cqt_b = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=cfg["n_bins_b"],
        bins_per_octave=cfg["n_bins_CQT"][0],
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=False
    )
    cqt_b = np.abs(cqt_b)
    cqt_b_compensated = cqt_b / cfg["n_bins_b"]

    stft_b = lb.core.stft(
        input_audio, n_fft=cfg["N_fft"], hop_length=cfg["hop_length"]
    )
    stft_b = np.abs(stft_b)
    stft_b_compensated = stft_b / cfg["N_fft"]

    print("\n\n##### TEST 1 #####")
    compare_single_frame(cqt_b, cqt_b_compensated, stft_b, stft_b_compensated, 10, True)
    compare_single_frame(cqt_b, cqt_b_compensated, stft_b, stft_b_compensated, 15, True)
    compare_single_frame(cqt_b, cqt_b_compensated, stft_b, stft_b_compensated, 20, True)
    compare_single_frame(cqt_b, cqt_b_compensated, stft_b, stft_b_compensated, 25, True)

############# TEST 2 ######################################################
# Compare stacked CQT amplitude with STFT amplitude
if run_test[1]:
    cqt_b = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=cfg["n_bins_b"],
        bins_per_octave=cfg["n_bins_CQT"][0],
        fmin=cfg["f_min_CQT"],
        scale=False
    )
    cqt_b = np.abs(cqt_b)
    cqt_b_compensated = cqt_b / cfg["n_bins_b"]

    cqt_m = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=cfg["n_bins_m"],
        bins_per_octave=cfg["n_bins_CQT"][1],
        fmin=cfg["f_min_CQT"],
        scale=False
    )
    cqt_m = np.abs(cqt_m)
    cqt_m_compensated = cqt_m / cfg["n_bins_m"]

    cqt_h = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=cfg["n_bins_h"],
        bins_per_octave=cfg["n_bins_CQT"][2],
        fmin=cfg["f_min_CQT"],
        scale=False
    )
    cqt_h = np.abs(cqt_h)
    cqt_h_compensated = cqt_h / cfg["n_bins_h"]

    cqt = np.vstack(
        [cqt_b[cfg["idx_b"][0]: cfg["idx_b"][1], :],
         cqt_m[cfg["idx_m"][0]: cfg["idx_m"][1], :],
         cqt_h[cfg["idx_h"][0]: cfg["idx_h"][1], :], ]
    )
    cqt_compensated = np.vstack(
        [cqt_b_compensated[cfg["idx_b"][0]: cfg["idx_b"][1], :],
         cqt_m_compensated[cfg["idx_m"][0]: cfg["idx_m"][1], :],
         cqt_h_compensated[cfg["idx_h"][0]: cfg["idx_h"][1], :], ]
    )

    f_max = cfg["f_min_CQT"] * np.power(2, (cfg["n_bins_h"] / cfg["n_bins_CQT"][2]))
    max_bin = int(round(f_max * cfg["N_fft"] / SAMPLE_RATE))

    stft = lb.core.stft(
        input_audio, n_fft=cfg["N_fft"], hop_length=cfg["hop_length"]
    )
    # stft = stft[1:max_bin, :]
    stft = np.abs(stft)
    stft_compensated = stft / cfg["N_fft"]

    print("\n\n##### TEST 2 #####")
    compare_single_frame(cqt, cqt_compensated, stft, stft_compensated, 10, True)
    compare_single_frame(cqt, cqt_compensated, stft, stft_compensated, 15, True)
    compare_single_frame(cqt, cqt_compensated, stft, stft_compensated, 20, True)
    compare_single_frame(cqt, cqt_compensated, stft, stft_compensated, 25, True)

############# TEST 3 ######################################################
# Compare scaled versions
if run_test[2]:
    cqt_b = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=cfg["n_bins_b"],
        bins_per_octave=cfg["n_bins_CQT"][0],
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=False,
        norm=None
    )
    # cqt = np.abs(cqt)
    # cqt_compensated = cqt / cfg["n_bins_b"]
    # cqt_compensated = cqt / cfg["n_bins_b"]

    # audio_frames = lb.util.frame(input_audio, frame_length=cfg["N_fft"], hop_length=cfg["hop_length"])
    # spectrum = np.zeros((int(cfg["N_fft"] / 2 + 1), audio_frames.shape[1]))
    # window = lb.filters.get_window('hann', Nx=cfg["N_fft"])
    #
    # for frame in range(audio_frames.shape[1]):
    #     input_signal = np.multiply(audio_frames[:, frame], window)
    #     spectrum[:, frame] = np.abs(np.fft.rfft(input_signal, n=cfg["N_fft"], norm=None))
    #
    # stft = spectrum
    # stft_compensated = stft / np.sqrt(cfg["N_fft"])
    # stft_b_compensated = spectrum / cfg["N_fft"]

    # stft_b = lb.core.stft(
    #     input_audio, n_fft=cfg["N_fft"], hop_length=cfg["hop_length"],
    # )
    # stft_b = np.abs(stft_b)
    # stft_b_compensated = stft_b / np.sqrt(cfg["N_fft"])
    # stft_b_compensated = stft_b / cfg["N_fft"]
    rms = lb.feature.rms(input_audio, frame_length=cfg["N_fft"], hop_length=cfg["hop_length"], center=False)

    print("\n\n##### TEST 3 #####")
    compare_single_frame(cqt, cqt_compensated, stft, rms, 10, True)
    compare_single_frame(cqt, cqt_compensated, stft, rms, 15, True)
    compare_single_frame(cqt, cqt_compensated, stft, rms, 20, True)
    compare_single_frame(cqt, cqt_compensated, stft, rms, 25, True)

############# TEST 4 ######################################################
if run_test[3]:
    cqt_b = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=cfg["n_bins_b"],
        bins_per_octave=cfg["n_bins_CQT"][0],
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=False,
    )
    cqt_b = np.abs(cqt_b)
    cqt_b_compensated = cqt_b / cfg["n_bins_b"]

    cqt_m = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=cfg["n_bins_m"],
        bins_per_octave=cfg["n_bins_CQT"][1],
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=False,
    )
    cqt_m = np.abs(cqt_m)
    cqt_m_compensated = cqt_m / cfg["n_bins_m"]

    cqt_h = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=cfg["n_bins_h"],
        bins_per_octave=cfg["n_bins_CQT"][2],
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=False,
    )
    cqt_h = np.abs(cqt_h)
    cqt_h_compensated = cqt_h / cfg["n_bins_h"]

    cqt = np.vstack(
        [cqt_b[cfg["idx_b"][0]: cfg["idx_b"][1], :],
         cqt_m[cfg["idx_m"][0]: cfg["idx_m"][1], :],
         cqt_h[cfg["idx_h"][0]: cfg["idx_h"][1], :], ]
    )
    cqt_compensated = np.vstack(
        [cqt_b_compensated[cfg["idx_b"][0]: cfg["idx_b"][1], :],
         cqt_m_compensated[cfg["idx_m"][0]: cfg["idx_m"][1], :],
         cqt_h_compensated[cfg["idx_h"][0]: cfg["idx_h"][1], :], ]
    )

    rms = lb.feature.rms(input_audio, frame_length=cfg["N_fft"], hop_length=cfg["hop_length"], center=False)

    print("\n\n##### TEST 4 #####")
    compare_single_frame(cqt, cqt_compensated, stft, rms, 10, True)
    compare_single_frame(cqt, cqt_compensated, stft, rms, 15, True)
    compare_single_frame(cqt, cqt_compensated, stft, rms, 20, True)
    compare_single_frame(cqt, cqt_compensated, stft, rms, 25, True)

############# TEST 5 ######################################################
# Now we use the real parseval!
# Sum of squared time values in frame = mean of absolute frequency values in frame

if run_test[4]:
    BINS_PER_OCTAVE = 12
    N_BINS = 12 * 8

    cqt = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=N_BINS,
        bins_per_octave=BINS_PER_OCTAVE,
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=False,
        norm=False,
        pad_mode="reflect",
    )
    cqt = np.abs(cqt)
    cqt = np.power(cqt, 2)

    dft_length = np.power(2, np.ceil(np.log2(N_BINS))).astype(int)
    cqt_compensated = cqt / dft_length

    # cqt = np.power(cqt, 2)
    # cqt_compensated = cqt / (N_BINS * N_BINS)

    # We can't turn off padding in cqt, so we must pad the time data for the time-amplitude
    padded_audio = np.pad(input_audio, pad_width=cfg["hop_length"], mode="reflect")

    frames = lb.util.frame(padded_audio, frame_length=cfg["N_fft"], hop_length=cfg["hop_length"],)
    n_frames = frames.shape[1]

    time_amplitude = np.zeros((1, n_frames))
    for frame in range(n_frames):
        time_amplitude[0, frame] = np.sum(np.power(frames[:, frame], 2))
        # time_amplitude[0, frame] = np.sqrt(np.sum(np.power(frames[:, frame], 2)))

    mean_difference = np.mean(cqt_compensated, axis=0) - time_amplitude[0, :]
    mean_difference_log = 20 * np.log10(mean_difference)
    plt.subplot(2, 1, 1)
    plt.title("Test 5")
    plt.plot(mean_difference)
    plt.plot(mean_difference_log)
    plt.legend(["diff", "log diff"])
    plt.subplot(2, 1, 2)
    plt.plot(input_audio)
    plt.show()

    print("\n\n##### TEST 5 - real parseval #####")
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude, 10, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude, 15, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude, 20, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude, 25, True)


############# TEST 6 ######################################################
# Use LibRosa's default CQT normalization.

if run_test[5]:
    BINS_PER_OCTAVE = 12
    N_BINS = 12 * 8

    # NOTE: scale=True needs to be set!!!
    #       otherwise the first few frames have way more/less energy!
    cqt = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=N_BINS,
        bins_per_octave=BINS_PER_OCTAVE,
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=True
    )
    cqt = np.abs(cqt)
    cqt_compensated = cqt

    # We can't turn off padding in cqt, so we must pad the time data for the time-amplitude
    padded_audio = np.pad(input_audio, pad_width=cfg["hop_length"], mode="reflect")
    frames = lb.util.frame(padded_audio, frame_length=cfg["N_fft"], hop_length=cfg["hop_length"],)
    n_frames = frames.shape[1]

    window_compensation = np.sum(lb.sequence.get_window("hann", cfg["N_fft"])) / cfg["N_fft"]

    time_amplitude = np.zeros((1, n_frames))
    for frame in range(n_frames):
        # time_amplitude[0, frame] = np.sum(np.power(frames[:, frame], 2))
        # time_amplitude[0, frame] = np.sqrt(np.sum(np.power(frames[:, frame], 2)))
        time_amplitude[0, frame] = np.sqrt(np.sum(np.power(frames[:, frame], 2)) / cfg["N_fft"])

    time_amplitude_compensated = time_amplitude * window_compensation

    # mean_difference = np.mean(cqt_compensated, axis=0) - time_amplitude[0, :]
    # mean_difference = np.sqrt(np.sum(cqt_compensated, axis=0) / cfg["N_fft"]) - time_amplitude_compensated[0, :]
    mean_difference = np.mean(cqt_compensated, axis=0) - time_amplitude[0, :]

    mean_difference_log = 20 * np.log10(mean_difference)
    plt.subplot(2, 1, 1)
    plt.title("Test 6")
    plt.plot(mean_difference)
    # plt.plot(20 * np.log10(np.abs(mean_difference_log)))
    plt.legend(["diff: CQT - RMS", "log abs diff: CQT - RMS"])
    plt.subplot(2, 1, 2)
    plt.plot(input_audio)
    plt.show()

    print("\n\n##### TEST 6 #####")
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 10, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 15, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 20, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 25, True)


############# TEST 7 ######################################################
# Use CQT filter lengths to normalize.

if run_test[6]:
    cqt = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=12 * 8,
        bins_per_octave=12,
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=False
    )
    cqt = np.abs(cqt)

    lengths = lb.filters.constant_q_lengths(sr=SAMPLE_RATE, fmin=cfg["f_min_CQT"],
        n_bins=12 * 8,
        bins_per_octave=12,
        window='hann',)

    cqt_compensated = cqt / lengths[:, np.newaxis]
    # cqt_compensated /= 128

    # padded_audio = np.pad(input_audio, pad_width=cfg["hop_length"], mode="reflect")
    # window = lb.sequence.get_window("hann", cfg["N_fft"])
    #
    # frames = lb.util.frame(padded_audio, frame_length=cfg["N_fft"], hop_length=cfg["hop_length"],)
    # n_frames = frames.shape[1]
    #
    # time_amplitude = np.zeros((1, n_frames))
    # time_amplitude_compensated = np.zeros((1, n_frames))
    # for frame in range(n_frames):
    #     time_amplitude[0, frame] = np.sum(np.power(frames[:, frame], 2)) / cfg["N_fft"]
    #     time_amplitude_compensated[0, frame] = np.sum(np.power(frames[:, frame] * window, 2))
        # time_amplitude_compensated[0, frame] = np.sum(np.power(frames[:, frame] * window, 2)) / cfg["N_fft"]

    time_amplitude = lb.core.stft(
        input_audio, n_fft=cfg["N_fft"], hop_length=cfg["hop_length"], win_length=cfg["N_fft"], window="hann",
        center=True, pad_mode="reflect"
    )
    time_amplitude = np.abs(time_amplitude)
    time_amplitude_compensated = time_amplitude / cfg["N_fft"]
    f_max_cqt = cfg["f_min_CQT"] * np.power(2, 8)
    bin_f_max_cqt = int(np.ceil(f_max_cqt * cfg["N_fft"] / SAMPLE_RATE))
    time_amplitude_compensated = time_amplitude_compensated[:bin_f_max_cqt, :]

    # mean_difference = np.mean(cqt_compensated, axis=0) - time_amplitude[0, :]
    # mean_difference = np.sqrt(np.sum(cqt_compensated, axis=0) / cfg["N_fft"]) - time_amplitude_compensated[0, :]
    # mean_difference = np.sum(cqt_compensated, axis=0) - time_amplitude_compensated[0, :]
    mean_difference = np.sum(cqt_compensated, axis=0) - np.sum(time_amplitude_compensated, axis=0)

    mean_difference_log = 20 * np.log10(mean_difference)
    plt.subplot(2, 1, 1)
    plt.title("Test 7")
    plt.plot(mean_difference)
    # plt.plot(20 * np.log10(np.abs(mean_difference_log)))
    plt.legend(["diff: CQT - RMS", "log abs diff: CQT - RMS"])
    plt.subplot(2, 1, 2)
    plt.plot(input_audio)
    plt.show()

    print("\n\n##### TEST 7 #####")
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 10, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 15, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 20, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 25, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 45, True)

############# TEST 8 ######################################################
# Use real parseval again and CQT lenghts to normalize CQT spec.

if run_test[7]:
    cqt = lb.cqt(
        input_audio,
        hop_length=cfg["hop_length"],
        n_bins=12 * 8,
        bins_per_octave=12,
        fmin=cfg["f_min_CQT"],
        window='hann',
        scale=True
    )
    cqt = np.abs(cqt)

    lengths = lb.filters.constant_q_lengths(sr=SAMPLE_RATE, fmin=cfg["f_min_CQT"],
        n_bins=12 * 8,
        bins_per_octave=12,
        window='hann',)

    cqt_compensated = cqt / lengths[:, np.newaxis]

    # For normalization see https://dsp.stackexchange.com/questions/49184/stft-amplitude-normalization-librosa-library
    window = np.hanning(cfg["N_fft"])
    time_amplitude = lb.core.stft(
        input_audio, n_fft=cfg["N_fft"], hop_length=cfg["hop_length"], win_length=cfg["N_fft"], window=window,
        center=True, pad_mode="reflect"
    )

    if True:
        f_max_cqt = cfg["f_min_CQT"] * np.power(2, 8)
        bin_f_max_cqt = int(np.ceil(f_max_cqt * cfg["N_fft"] / SAMPLE_RATE))
        time_amplitude = time_amplitude[:bin_f_max_cqt, :]

    time_amplitude = 2 * np.abs(time_amplitude) / np.sum(window)
    # time_amplitude = np.square(time_amplitude)
    time_amplitude = np.sum(time_amplitude, axis=0)
    time_amplitude = np.reshape(time_amplitude, [1, time_amplitude.shape[0]])
    time_amplitude_compensated = time_amplitude / cfg["N_fft"]
    # time_amplitude_compensated /= 2

    cqt_env = np.sum(cqt_compensated, axis=0)[:]
    stft_env = time_amplitude_compensated[0, :]

    mean_difference = cqt_env - stft_env

    mean_difference_log = 20 * np.log10(mean_difference)
    plt.subplot(2, 1, 1)
    plt.title("Test 8")
    plt.plot(mean_difference_log)
    # plt.plot(20 * np.log10(np.abs(mean_difference_log)))
    plt.legend(["diff: CQT - RMS", "log abs diff: CQT - RMS"])
    plt.subplot(2, 1, 2)
    plt.plot(input_audio)
    plt.show()

    print("\n\n##### TEST 7 #####")
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 10, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 15, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 20, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 25, True)
    compare_single_frame(cqt, cqt_compensated, time_amplitude, time_amplitude_compensated, 45, True)


# cqt = lb.cqt(
#     input_audio,
#     hop_length=cfg["hop_length"],
#     n_bins=24 * 8,
#     bins_per_octave=24,
#     fmin=cfg["f_min_CQT"],
#     window='hann',
#     scale=True
# )
# lengths_2 = lb.filters.constant_q_lengths(sr=SAMPLE_RATE, fmin=cfg["f_min_CQT"], n_bins=24 * 8,
#     bins_per_octave=24,
#     window='hann',)

