from librosa import frames_to_time
from csv import writer as csv_writer
from os import path


def saveSingleCsvFile(fileName, data, filePermission="w"):
    with open(fileName, filePermission) as csvFile:
        writer = csv_writer(csvFile)
        writer.writerows(data)
        csvFile.close()


def save_results_to_csv(
    cfg,
    data,
    activations,
    X_ctrl,
    beats_t,
    output_path="output_activations",
    refurbished=False,
):
    for inst_index, inst in enumerate(data.instrument_list):
        csvData = []
        for f in range(activations.shape[1]):
            csvData.append(
                [
                    frames_to_time(
                        f,
                        sr=cfg["sr"],
                        hop_length=cfg["hop_length"],
                        n_fft=cfg["N_fft"],
                    ),
                    activations[inst_index, f],
                ]
            )

        if refurbished:
            saveSingleCsvFile(path.join(output_path, inst + ".csv"), csvData)
        else:
            saveSingleCsvFile(
                path.join(output_path, inst + "_refurbished.csv"), csvData
            )

    csvData = []
    for f in range(X_ctrl.size):
        csvData.append(
            [
                frames_to_time(
                    f, sr=cfg["sr"], hop_length=cfg["hop_length"], n_fft=cfg["N_fft"]
                ),
                X_ctrl[f],
            ]
        )

    saveSingleCsvFile(path.join(output_path, "X_ctrl.csv"), csvData)

    csvData = list(beats_t)
    saveSingleCsvFile(path.join(output_path, "beats.csv"), map(lambda x: [x], csvData))
