import matplotlib.pyplot as plt
import librosa as lb
import numpy as np
from plotly.subplots import make_subplots
import plotly.graph_objects as go


def plot_envelope(X_ctrl, X_eval, X_error, t_sound_file, cfg, title="Envelopes"):
    # define params
    min_db = -140
    lw = 2.5
    # Init plot
    fig = go.Figure()

    # Set font size
    fig.layout.font.size = cfg["plot"]["font_size"]

    x_vals = np.arange(0, len(X_ctrl), 1) * t_sound_file / len(X_ctrl)
    if cfg["plot"]["envelope_scale"] == "log":
        y_1 = 2 * lb.core.amplitude_to_db(X_ctrl)
        y_1[y_1 < min_db] = min_db
        y_2 = 2 * lb.core.amplitude_to_db(X_eval)
        y_2[y_2 < min_db] = min_db
        y_3 = 2 * lb.core.amplitude_to_db(X_error)
        y_3[y_3 < min_db] = min_db
        ylabel = "Amplitude / db"
    else:
        y_1 = X_ctrl
        y_2 = X_eval
        y_3 = X_error
        ylabel = "Amplitude"

    fig.add_trace(
        go.Scatter(
            x=x_vals, y=y_1, name="Reference", line=dict(color="royalblue", width=lw)
        )
    )
    fig.add_trace(
        go.Scatter(
            x=x_vals, y=y_2, name="Prediction", line=dict(color="firebrick", width=lw)
        )
    )
    fig.add_trace(
        go.Scatter(x=x_vals, y=y_3, name="Error", line=dict(color="green", width=lw))
    )

    fig.update_layout(
        title_x=0.5, xaxis_title="Time / s", yaxis_title=ylabel
    )

    if cfg["plot"]["write_figure_title"]:
        fig.update_layout(title_text=title,)
        fig.layout.title.font.size = cfg["plot"]["font_size"]

    fig.show()

    return fig


def plot_envelope_comparison(
    X_ctrl,
    X_eval,
    X_error,
    X_eval_post,
    X_error_post,
    t_sound_file,
    cfg,
    title=["1", "2"],
):
    # define params
    min_db = -140
    lw = 2.5
    # init plot
    fig = make_subplots(
        rows=2, cols=1, subplot_titles=title, shared_xaxes=True, vertical_spacing=0.05
    )

    # Set font size
    fig.layout.font.size = cfg["plot"]["font_size"]

    # define values
    x_vals = np.arange(0, len(X_ctrl), 1) * t_sound_file / len(X_ctrl)
    if cfg["plot"]["envelope_scale"] == "log":
        y_1 = 2 * lb.core.amplitude_to_db(X_ctrl)
        y_1[y_1 < min_db] = min_db
        y_2 = 2 * lb.core.amplitude_to_db(X_eval)
        y_2[y_2 < min_db] = min_db
        y_3 = 2 * lb.core.amplitude_to_db(X_error)
        y_3[y_3 < min_db] = min_db
        y_4 = 2 * lb.core.amplitude_to_db(X_eval_post)
        y_4[y_4 < min_db] = min_db
        y_5 = 2 * lb.core.amplitude_to_db(X_error_post)
        y_5[y_5 < min_db] = min_db
        ylabel = "Amplitude / db"
    else:
        y_1 = X_ctrl
        y_2 = X_eval
        y_3 = X_error
        y_4 = X_eval_post
        y_5 = X_error_post
        ylabel = "Amplitude"

    # Subplot 1
    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=y_1,
            legendgroup="group1",
            name="Reference",
            line=dict(color="royalblue", width=lw),
        ),
        row=1,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=y_2,
            legendgroup="group2",
            name="Prediction",
            line=dict(color="firebrick", width=lw),
        ),
        row=1,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=y_3,
            legendgroup="group3",
            name="Error",
            line=dict(color="green", width=lw),
        ),
        row=1,
        col=1,
    )
    # Subplot 2
    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=y_1,
            legendgroup="group1",
            name="Reference",
            showlegend=False,
            line=dict(color="royalblue", width=lw),
        ),
        row=2,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=y_4,
            legendgroup="group2",
            name="Prediction",
            showlegend=False,
            line=dict(color="firebrick", width=lw),
        ),
        row=2,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=y_5,
            legendgroup="group3",
            name="Error",
            showlegend=False,
            line=dict(color="green", width=lw),
        ),
        row=2,
        col=1,
    )

    fig.update_xaxes(title_text="time / ms", row=2, col=1)
    fig.update_yaxes(title_text=ylabel, row=1, col=1)
    fig.update_yaxes(title_text=ylabel, row=2, col=1)

    if cfg["plot"]["write_figure_title"]:
        fig.update_layout(title_text=title)
        fig.layout.title.font.size = cfg["plot"]["font_size"]

    fig.show()

    return fig


def plot_activation_comparison(
    activations_pre, activations, cfg, t_sound_file, title=["1", "2"]
):
    # calc SNR (if sound file is a test file)
    SNR_string_pre = calcSNR(activations_pre, cfg)
    SNR_string = calcSNR(activations, cfg)
    # append SNR to titles
    title = [title[0] + SNR_string_pre, title[1] + SNR_string]
    # define params
    min_db = cfg["plot"]["min_db_heatmap"]
    # Init plot
    fig = make_subplots(
        rows=2, cols=1, subplot_titles=title, shared_xaxes=True, vertical_spacing=0.05
    )

    # Set font size
    fig.layout.font.size = cfg["plot"]["font_size"]

    # define values
    x_vals = (
        np.arange(0, np.shape(activations_pre)[1], 1)
        * t_sound_file
        / np.shape(activations_pre)[1]
    )
    y_vals = cfg["instrument_list"]
    if y_vals is None or len(y_vals) == 0:  # TODOworkaround for now
        y_vals = ["1", "2", "3", "4", "5", "6", "7"]
    if cfg["plot"]["activation_scale"] == "log":
        z_1 = 2 * lb.core.amplitude_to_db(activations_pre)
        z_1[z_1 < min_db] = min_db
        z_2 = 2 * lb.core.amplitude_to_db(activations)
        z_2[z_2 < min_db] = min_db
    else:
        z_1 = activations_pre
        z_2 = activations
    # plot
    fig.append_trace(
        go.Heatmap(z=z_1, x=x_vals, y=y_vals, coloraxis="coloraxis"), row=1, col=1
    )
    fig.append_trace(
        go.Heatmap(z=z_2, x=x_vals, y=y_vals, coloraxis="coloraxis"), row=2, col=1
    )
    fig.update_xaxes(title_text="time / ms", row=2, col=1)
    fig.update_yaxes(title_text="instrument", row=1, col=1)
    fig.update_yaxes(title_text="instrument", row=2, col=1)
    fig.update_layout(
        coloraxis=dict(
            cmax=min_db,
            cmin=0,
            colorscale="Viridis",
            colorbar=dict(
                title="Amplitude / db",
                tickvals=np.linspace(min_db, 0, 9, endpoint=True),
            ),
        )
    )

    if cfg["plot"]["write_figure_title"]:
        fig.update_layout(title_text=title)
        fig.layout.title.font.size = cfg["plot"]["font_size"]

    fig.show()

    return fig


def plot_activations(activations, cfg, t_sound_file, title="1"):
    # calc SNR (if sound file is a test file)
    SNR_string = calcSNR(activations, cfg)
    # append SNR to titles
    title = title + SNR_string
    # define params
    min_db = cfg["plot"]["min_db_heatmap"]
    # Init plot
    fig = go.Figure()

    # Set font size
    fig.layout.font.size = cfg["plot"]["font_size"]

    # define values
    x_vals = (
        np.arange(0, np.shape(activations)[1], 1)
        * t_sound_file
        / np.shape(activations)[1]
    )
    y_vals = cfg["instrument_list"]
    if y_vals is None or len(y_vals) == 0:  # TODOworkaround for now
        y_vals = ["1", "2", "3", "4", "5", "6", "7"]

    if cfg["plot"]["activation_scale"] == "log":
        z = 20 * np.log10(activations)
        z[z < min_db] = min_db
        cmin = min_db
        cmax = 0.0
        tickvals = np.linspace(min_db, 0, 9, endpoint=True)
    else:
        z = activations
        cmin = 0.0
        cmax = 1.0
        tickvals = np.linspace(0.0, 1.0, 9, endpoint=True)

    # plot
    fig.add_trace(go.Heatmap(z=z, x=x_vals, y=y_vals, coloraxis="coloraxis"))
    fig.update_xaxes(title_text="time / ms")
    fig.update_yaxes(title_text="instrument")

    fig.update_layout(
        coloraxis=dict(
            cmax=cmin,
            cmin=cmax,
            colorscale="Viridis",
            colorbar=dict(
                title="Amplitude / db",
                tickvals=tickvals,
            ),
        ),
        title_x=0.5,
    )

    if cfg["plot"]["write_figure_title"]:
        fig.update_layout(title_text=title)
        fig.layout.title.font.size = cfg["plot"]["font_size"]

    fig.show()

    return fig


def calcSNR(activation, cfg):
    SNR_string = ""
    file_name = cfg["input_file_path"]

    # check if sound file is a test file
    if file_name.find("test_") != -1:
        inst = file_name.split("_")[1]
        if inst in cfg["instrument_list"]:
            # get row indices of activation matrix
            signal_idx = np.where(np.array(cfg["instrument_list"]) == inst)[0]
            noise_idx = np.where(np.array(cfg["instrument_list"]) != inst)[0]
            # calculate signal and noise power
            signal = np.sum(np.sum(activation[signal_idx, :], 0))
            noise = np.sum(np.sum(activation[noise_idx, :], 0))
            # calc SNR and gen string
            SNR = 20 * np.log10(signal / noise)
            SNR_string = ", SNR = " + str("{:.2f}".format(SNR) + " dB")
        else:
            SNR_string = ", SNR = ? dB"

    return SNR_string
