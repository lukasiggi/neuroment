import numpy as np


def average_output_matrix_frames(y_pred):
    """Concatenate frames by meaning the overlapping segments.

    Args:
        y_pred (np.ndarray): Set of prediction matrices

    Returns:
        np.ndarray: Concatenated output matrix
    """
    eps = np.finfo(float).eps
    # TODO switch the dimensions for clarity
    out = np.zeros((y_pred.shape[2], y_pred.shape[1]))
    lost_frames = 0
    for i in range(y_pred.shape[1]):
        buffer = y_pred[:, i, :]
        buffer = np.delete(buffer, np.where(~buffer.any(axis=1))[0], axis=0)
        try:
            if np.all(buffer < eps):
                # prevent error for mean of empty slices
                out[:, i] = np.zeros(buffer.shape)
            else:
                out[:, i] = np.mean(buffer, axis=0)
        except:
            lost_frames += 1

    # TODO fix me
    out[np.where(np.isnan(out))] = 0.0
    print("{} frames lost".format(lost_frames))
    return out
