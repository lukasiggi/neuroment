import numpy as np
from skimage.filters import threshold_otsu
from scipy.signal import medfilt
import librosa as lb
import matplotlib.pyplot as plt


def tanh_refurbish(activations, ctrl_envelope, eval_envelope, amp_factor=1):
    """refurbishes the output activations with additional information about the input signal

    Args:
        activations (np.ndarray): output activation matrix
        ctrl_envelope (np.ndarray): envelope of the input
        eval_envelope (np.ndarray): envelope of the evaluated output
        amp_factor (int, optional): Defaults to 1.

    Returns:
        np.ndarray: refurbished activation
    """
    out = activations.copy()

    # define params
    min_db = -140
    max_db = 0

    # scale activations logarithmically
    out_log = 2 * lb.core.amplitude_to_db(activations_refurbished)
    out_log[out_log < min_db] = min_db

    # OTSU
    eps = 0.0000001
    otsu_thresh = np.zeros(out_log.shape[1]) + min_db
    for fr in range(out_log.shape[1]):
        if eval_envelope[fr] > eps and np.any(out_log[:, fr] > min_db):
            otsu_thresh[fr] = threshold_otsu(out_log[:, fr])

    offset = 1  # y offset
    x_factor = 0.5  # controls steepness of curve

    # calc amplitude factor
    # A < ... curve is compressed, A > ... curve is expanded
    # e.g. A = 0.5 -> scale between -6 and +6 dB
    A = ctrl_envelope / np.max(ctrl_envelope)
    A = medfilt(A, 11) * amp_factor

    # calc shift factor via OTSU to symmetrize tanh around OTSU threshold
    # shift needs to be mapped into -5...+5
    shift = medfilt(otsu_thresh, 11) * (10 / (max_db - min_db)) + 5

    # map out_log into -5...+5 (active range of tanh)
    x = out_log * (10 / (max_db - min_db)) + 5

    for fr in range(out.shape[1]):
        out[:, fr] *= offset + A[fr] * np.tanh(x_factor * (x[:, fr] - shift[fr]))

    return out


def threshold_refurbish(activations, ctrl_envelope, eval_envelope, X_error):
    # OTSU
    eps = 0.0000001
    thresh = np.zeros(activations.shape[1])
    for fr in range(activations.shape[1]):
        if eval_envelope[fr] > eps:
            thresh[fr] = threshold_otsu(activations[:, fr])

    # thresh = medfilt(thresh, 5)
    volume_thresh = medfilt(ctrl_envelope, 25)

    ctr_ind = np.zeros(activations.shape[1])
    for fr in range(activations.shape[1]):
        # sort by size (ascending)
        sort_ind = np.argsort(activations[:, fr])
        sum_act = 0

        if (
            eval_envelope[fr] > volume_thresh[fr]
            and (X_error[fr] > eps)
            and (eval_envelope[fr] > eps)
        ):
            ind_otsu = activations[:, fr] > thresh[fr]
            ctr_ind[fr] = np.sum(ind_otsu)
            tmp = np.sum(activations[ind_otsu, fr])
            alpha = ctrl_envelope[fr] / tmp
            activations[ind_otsu, fr] *= alpha
            thresh[fr] = threshold_otsu(activations[:, fr])
        for ii, instr in enumerate(sort_ind):
            sum_act += activations[instr, fr]
            if sum_act >= X_error[fr] or activations[instr, fr] > thresh[fr]:
                activations[sort_ind[0:ii], fr] = 0
                break

    return activations
