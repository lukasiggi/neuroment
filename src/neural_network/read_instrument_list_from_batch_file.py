import pickle
from os.path import join


# TODO implement another way of reading the instrument list (this one is pretty inefficient)
def read_instrument_list_from_batch_file(cfg, batch_file_list):
    """
    Reads the instrument list for cfg from the batch files in batch_file_list.

    The batch files are opened one after the other. Each time the function finds a new instrument in a batch file
    it adds the instrument to the new instrument list. The new instrument list then replaces the one in cfg.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file
        batch_file_list (list): file names of each batch file (no full path)

    Returns:
        cfg (dict): configuration parameters with the updated instrument list
    """

    instrument_list = []

    for file in batch_file_list:
        file_path = join(cfg["dir_dataset"], file)

        with open(file_path, "rb") as f:
            samples = pickle.load(f)

            for sample in samples:
                if sample.id["filename"] not in instrument_list:
                    instrument_list.append(sample.id["filename"])

    for idx, inst in enumerate(instrument_list):
        if len(inst) < 2:
            instrument_list.pop(idx)

    cfg["instrument_list"] = instrument_list

    return cfg
