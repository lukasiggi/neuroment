from tensorflow.keras.layers import (
    Input,
    Dense,
    Conv2D,
    Dropout,
    Flatten,
    Reshape,
    concatenate,
    BatchNormalization,
    ReLU,
    MaxPool2D,
)
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, TerminateOnNaN, EarlyStopping
from tensorflow import metrics
from tensorflow import losses
from tensorflow import optimizers
from tensorflow import reduce_mean
from tensorflow import image
from tensorflow.keras.applications import DenseNet121


def create_model_and_callbacks(cfg):
    """
    Creates and returns a desired Keras model and empty Tensorboard callbacks.

    The type of model that will be created can be set in the config file.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file

    Returns:
        model (Keras functional): newly created Keras model
        keras_callbacks (Keras tensorboard): empty Keras callbacks
    """

    if cfg["model_to_create"] == "custom":
        structure = get_2D_cnn_structure(cfg)
        model = init_custom_2D_cnn_model(cfg, structure)

    elif cfg["model_to_create"] == "dense_net":
        model = init_dense_net_model(cfg)

    elif cfg["model_to_create"] == "huzaifah_net":
        structure = get_2D_cnn_structure(cfg)
        model = init_huzaifah_net_model(cfg, structure)

    else:
        raise Exception(
            "Model to create is not known (see config variable model_to_create for available models)."
        )

    keras_callbacks = init_keras_callbacks(cfg)

    return model, keras_callbacks


def init_keras_callbacks(cfg):
    """
    Initializes the Keras callbacks used in model fitting and prediction.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file

    Returns:
        keras_callbacks (Keras tensorboard): empty Keras callbacks
    """


    callbacks = [
        EarlyStopping(
            monitor="val_loss",
            mode="min",
            patience=cfg["patience"],
            min_delta=cfg["min_delta"],
            restore_best_weights=True,
        ),
        TensorBoard(
            log_dir=cfg["tensorboard_log_path"],
            update_freq="epoch",
            histogram_freq=1,
            write_graph=True,
            write_images=True,
        ),
        TerminateOnNaN()
    ]

    # create checkpoints for a certain number of epochs
    checkpoint_path = "models/checkpoint_%s.h5" % cfg["time_string"]
    callbacks.append(
        ModelCheckpoint(
            filepath=checkpoint_path,
            monitor="val_loss",
            mode="min",
            save_best_only=True,
            save_weights_only=False,
            save_freq="epoch",
        )
    )

    print("Logging tensorboard to: " + cfg["tensorboard_log_path"])

    return callbacks


def init_custom_2D_cnn_model(cfg, structure):
    """
    Initializes the custom 2D CNN model designed explicitly for Neuroment.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file
        structure (list): nested list which defines the model dimensions

    Returns:
        model (Keras functional): newly created Keras model

    Note:
        - Useful references on metrics:
            - List of metrics: https://www.tensorflow.org/api_docs/python/tf/keras/metrics
            - How to handle them: https://keras.io/api/metrics/
        - Useful references for losses:
            - How to handle them: https://keras.io/api/losses/
    """

    # get input layer dimensions
    n_filter_input = structure[0][0]
    bottom_shape = structure[0][1]
    middle_shape = structure[0][2]
    top_shape = structure[0][3]

    # reshape_bottom = n_filter_input * bottom_shape[0] * bottom_shape[1]
    # reshape_middle = n_filter_input * middle_shape[0] * middle_shape[1]
    # reshape_top = n_filter_input * top_shape[0] * top_shape[1]

    N_CHANNELS = [16, 32, 32, 64, 64, 96]

    # create input layers
    cqt_stack_bottom = Input(shape=bottom_shape, name="bottom_input")
    cqt_stack_middle = Input(shape=middle_shape, name="middle_input")
    cqt_stack_top = Input(shape=top_shape, name="top_input")

    # create all layers for bass-frequency analysis
    bottom = Conv2D(N_CHANNELS[0], (3, 3), activation="linear", padding="same")(cqt_stack_bottom)
    bottom = BatchNormalization()(bottom)
    bottom = ReLU()(bottom)
    bottom = MaxPool2D((2, 2))(bottom)
    bottom = Conv2D(N_CHANNELS[1], (3, 3), activation="linear")(bottom)
    bottom = BatchNormalization()(bottom)
    bottom = ReLU()(bottom)
    # bottom = MaxPool2D((2, 2))(bottom)
    bottom = Conv2D(N_CHANNELS[2], (5, 5), activation="linear", padding="same")(bottom)
    bottom = BatchNormalization()(bottom)
    bottom = ReLU()(bottom)
    bottom = Conv2D(N_CHANNELS[3], (5, 5), activation="linear")(bottom)
    bottom = BatchNormalization()(bottom)
    bottom = ReLU()(bottom)
    # bottom = MaxPool2D((2, 2))(bottom)
    bottom = Conv2D(N_CHANNELS[4], (7, 7), activation="linear", padding="same")(bottom)
    bottom = BatchNormalization()(bottom)
    bottom = ReLU()(bottom)
    bottom = Conv2D(N_CHANNELS[5], (7, 7), activation="linear")(bottom)
    bottom = BatchNormalization()(bottom)
    bottom = ReLU()(bottom)
    bottom = Flatten()(bottom)

    # create all layers for mid-frequency analysis
    middle = Conv2D(N_CHANNELS[0], (3, 3), activation="linear", padding="same")(cqt_stack_middle)
    middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    middle = MaxPool2D((2, 2))(middle)
    middle = Conv2D(N_CHANNELS[1], (3, 3), activation="linear")(middle)
    middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    # middle = MaxPool2D((2, 1))(middle)
    middle = Conv2D(N_CHANNELS[2], (5, 5), activation="linear", padding="same")(middle)
    middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    middle = Conv2D(N_CHANNELS[3], (5, 5), activation="linear")(middle)
    middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    # middle = MaxPool2D((2, 2))(middle)
    middle = Conv2D(N_CHANNELS[4], (7, 7), activation="linear", padding="same")(middle)
    middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    middle = Conv2D(N_CHANNELS[5], (7, 7), activation="linear")(middle)
    middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    middle = Flatten()(middle)

    # create all layers for high-frequency analysis
    top = Conv2D(N_CHANNELS[0], (3, 3), activation="linear", padding="same")(cqt_stack_top)
    top = BatchNormalization()(top)
    top = ReLU()(top)
    top = MaxPool2D((2, 2))(top)
    top = Conv2D(N_CHANNELS[1], (3, 3), activation="linear")(top)
    top = BatchNormalization()(top)
    top = ReLU()(top)
    top = MaxPool2D((2, 1))(top)  # we have a really big height here
    top = Conv2D(N_CHANNELS[2], (5, 5), activation="linear", padding="same")(top)
    top = BatchNormalization()(top)
    top = ReLU()(top)
    top = Conv2D(N_CHANNELS[3], (5, 5), activation="linear")(top)
    top = BatchNormalization()(top)
    top = ReLU()(top)
    # top = MaxPool2D((2, 2))(top)
    top = Conv2D(N_CHANNELS[4], (7, 7), activation="linear", padding="same")(top)
    top = BatchNormalization()(top)
    top = ReLU()(top)
    top = Conv2D(N_CHANNELS[5], (7, 7), activation="linear")(top)
    top = BatchNormalization()(top)
    top = ReLU()(top)
    top = Flatten()(top)

    # concatenate bass-, mid- and high-frequency layers
    h = concatenate([bottom, middle, top])

    # get output layer parameters from structure(last element).
    shape_output = structure[-1]
    n = 1
    for i in shape_output:
        n *= i

    # create output layers
    h = Dense(n, activation="sigmoid")(h)  # afterwards; (None, 160)
    predictions = Reshape((shape_output[0], shape_output[1]))(h)

    # define model name via instrument list
    model_name = ".".join(cfg["instrument_list"])

    # create and compile model
    model = Model(
        inputs=[cqt_stack_bottom, cqt_stack_middle, cqt_stack_top],
        outputs=predictions,
        name=model_name,
    )

    model.compile(
        optimizer=optimizers.Nadam(),
        loss=[losses.MeanSquaredError()],
        metrics=[
            metrics.MeanSquaredError(),
            metrics.RootMeanSquaredError(),
            metrics.MeanAbsoluteError(),
        ],
        run_eagerly=True,
    )

    return model


def get_easy_cnn_structure(cfg, input_shape, n_instruments, n_frames_per_sample, with_batch_norm=False):
    N_CHAN_1 = 16
    N_CHAN_2 = 32
    N_CHAN_3 = 48
    N_CHAN_4 = 64

    input_layer = Input(shape=input_shape)

    # create all layers for mid-frequency analysis
    middle = Conv2D(N_CHAN_1, (3, 3), activation="linear")(input_layer)
    if with_batch_norm:
        middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    middle = MaxPool2D((2, 1))(middle)
    middle = Conv2D(N_CHAN_2, (3, 3), activation="linear")(middle)
    if with_batch_norm:
        middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    middle = MaxPool2D((2, 1))(middle)
    middle = Conv2D(N_CHAN_3, (5, 5), activation="linear")(middle)
    if with_batch_norm:
        middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    middle = MaxPool2D((2, 1))(middle)
    middle = Conv2D(N_CHAN_4, (7, 7), activation="linear")(middle)
    if with_batch_norm:
        middle = BatchNormalization()(middle)
    middle = ReLU()(middle)
    # middle = Flatten()(middle)
    middle = Flatten()(middle)

    output = Dense(n_instruments * n_frames_per_sample, activation="sigmoid")(middle)  # afterwards; (None, 160)
    output = Reshape((n_frames_per_sample, n_instruments))(output)

    # create and compile model
    model = Model(
        inputs=input_layer,
        outputs=output,
        name="easy_cnn_neuroment",
    )

    model.compile(
        optimizer=optimizers.Nadam(),
        loss=[losses.MeanSquaredError()],
        metrics=[
            metrics.MeanSquaredError(),
            metrics.RootMeanSquaredError(),
            metrics.MeanAbsoluteError(),
        ],
        run_eagerly=True,
    )

    return model

def get_2D_cnn_structure(cfg):
    """
    Defines and returns the dimensional structure of the CNN.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file

    Returns:
        structure (list): nested list which defines the model dimensions
    """

    print("New CNN generated.")

    # the output layer is n_filter x n_outputs
    structure = [
        [
            8,
            (cfg["n_bins_CQT"][0] * cfg["n_octaves_CQT"][0], cfg["n_frames"], 1),
            (cfg["n_bins_CQT"][1] * cfg["n_octaves_CQT"][1], cfg["n_frames"], 1),
            (cfg["n_bins_CQT"][2] * cfg["n_octaves_CQT"][2], cfg["n_frames"], 1),
        ],
        [cfg["num_instruments"], cfg["n_frames"]],
    ]

    return structure


def init_huzaifah_net_model(cfg, structure):
    """
    Initializes the huzaifah_net model.

    This model is experimental, and therefore not recommended!

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file
        structure (list): nested list which defines the model dimensions

    Returns:
        model (Keras functional): newly created Keras model
    """

    # get input layer dimensions
    n_filter_input = structure[0][0]
    bottom_shape = structure[0][1]
    middle_shape = structure[0][2]
    top_shape = structure[0][3]

    # create input layers
    cqt_stack_bottom = Input(shape=bottom_shape, name="bottom_input")
    cqt_stack_middle = Input(shape=middle_shape, name="middle_input")
    cqt_stack_top = Input(shape=top_shape, name="top_input")

    # create base-, mid- and high-frequency layers
    bottom = Conv2D(96, (bottom_shape[0], 3), activation="relu")(cqt_stack_bottom)
    bottom = Dropout(0.5)(bottom)

    middle = Conv2D(96, (middle_shape[0], 3), activation="relu")(cqt_stack_middle)
    middle = Dropout(0.5)(middle)

    top = Conv2D(96, (top_shape[0], 3), activation="relu")(cqt_stack_top)
    top = Dropout(0.5)(top)

    bottom = Flatten()(bottom)
    middle = Flatten()(middle)
    top = Flatten()(top)

    # concatenate the 3 layer collections
    h = concatenate([bottom, middle, top])

    # get output layer dimension
    shape_output = structure[-1]
    n = 1
    for i in shape_output:
        n *= i

    h = Dense(n, activation="sigmoid")(h)  # afterwards; (None, 160)
    predictions = Reshape((shape_output[0], shape_output[1]))(h)

    # generate model name
    model_name = ".".join(cfg["instrument_list"])

    # create and compile model
    model = Model(
        inputs=[cqt_stack_bottom, cqt_stack_middle, cqt_stack_top],
        outputs=predictions,
        name=model_name,
    )

    model.compile(
        optimizer=optimizers.Nadam(),
        loss=[losses.MeanSquaredError()],
        metrics=[
            metrics.MeanAbsoluteError(),
            metrics.MeanAbsolutePercentageError(),
            metrics.MeanSquaredError(),
            metrics.RootMeanSquaredError(),
            metrics.Accuracy(),
        ],
        run_eagerly=True,
    )

    return model


def init_dense_net_model(cfg):
    """
    Initializes the huzaifah_net model.

    This model is experimental, and therefore not recommended!

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file
        structure (list): nested list which defines the model dimensions

    Returns:
        model (Keras functional): newly created Keras model

    Note:
        dense_net model is derived from here:
          - https://www.pluralsight.com/guides/introduction-to-densenet-with-tensorflow
    """

    # define dimensions
    input_layer_height = (
        cfg["n_bins_CQT"][0] * cfg["n_octaves_CQT"][0]
        + cfg["n_bins_CQT"][1] * cfg["n_octaves_CQT"][1]
        + cfg["n_bins_CQT"][2] * cfg["n_octaves_CQT"][2]
    )
    input_layer_width = cfg["n_frames"]
    input_depth = 3  # but we only have 1 channel! -> copy this one later 2 times

    input_shape = (input_layer_height, input_layer_width, input_depth)

    # do not use pre-trained weights
    weights = None

    # generate dense_net
    model_d = DenseNet121(weights=weights, include_top=False, input_shape=input_shape)
    x = model_d.output

    num_instruments = len(cfg["instrument_list"])
    num_dense_layer_neurons = num_instruments * input_layer_width

    # TODO implement this for an arbitrary number of instruments (at the moment hard-coded to 7)
    x = concatenate(
        [
            x[:, 0, :, :],
            x[:, 1, :, :],
            x[:, 2, :, :],
            x[:, 3, :, :],
            x[:, 4, :, :],
            x[:, 5, :, :],
            x[:, 6, :, :],
        ]
    )

    # after this operation the model has shape (None, 7, 1, 1024)
    x = Dense(num_dense_layer_neurons, activation="sigmoid")(x)

    # generate prediction layer
    predictions = Reshape((num_instruments, input_layer_width))(x)

    # define model name
    model_name = ".".join(cfg["instrument_list"])

    # generate and compile final model
    model = Model(inputs=model_d.input, outputs=predictions, name=model_name)

    model.compile(
        optimizer="Adam", loss="categorical_crossentropy", metrics=["accuracy"]
    )

    return model  # output_shape needs to be (None, n_instruments, 32)


def ssim_loss(y_true, y_pred):
    return 1 - reduce_mean(image.ssim(y_true, y_pred, 1.0))
