from os.path import join
from time import gmtime, strftime


def save_model(model, cfg, cnn_file_path=""):
    """
    Saves a Keras model to hard drive.

    Args:
        model (Keras functional): newly created Keras model
        cfg (dict): configuration parameters, derived from a YAML configuration file
        cnn_file_path (str): absolute or relative path where model shall be saved to
    """

    try:
        # define a cnn_file_path if none was passed using timestamp
        if not cnn_file_path:
            cnn_file_path = join(
                cfg["model_output_path"],
                cfg["model_to_create"] + "_" + cfg["time_string"] + ".h5",
            )

        model.save(cnn_file_path)
        print("Saved CNN model to " + cnn_file_path + ".")

    except:
        print("Could not save model.")
