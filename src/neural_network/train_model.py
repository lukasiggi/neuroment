import numpy as np
import random
from os.path import join
import pickle
import gc
import tensorflow as tf
from copy import deepcopy
import json
from pprint import pprint

from src.neural_network.model_data_loader import ModelDataLoader

def train_model(cfg, batch_file_list, model, data, keras_callbacks):
    """
    Trains a given CNN model in one step or batch-wise.

    Args:
        cfg (dict): configuration parameters, derived from a YAML configuration file
        batch_file_list (list): file names of each batch file (no full path)
        model (Keras functional): Keras CNN model
        data (SampleSet): audio sample set, handles training data
        keras_callbacks (Keras tensorboard): Keras callbacks, passed into model.fit()
    """

    if cfg["batch_learning"]:
        if cfg["n_epochs_warmup"] > 0:
            base_batches = []
            for batch in batch_file_list:
                if batch.startswith("base_"):
                    base_batches.append(batch)

            print("Warming up model with %d epochs ..." % cfg["n_epochs_warmup"])

            train_files, test_files, val_files = split_into_train_test_val(cfg, deepcopy(base_batches))

            orig_n_mix_batches_per_epoch = cfg["n_mix_batches_per_epoch"]
            cfg["n_mix_batches_per_epoch"] = 0

            train_loader = ModelDataLoader(cfg, data, train_files)
            test_loader = ModelDataLoader(cfg, data, test_files)
            val_loader = ModelDataLoader(cfg, data, val_files)

            warmup_train_history = model.fit(
                train_loader,
                validation_data=val_loader,
                epochs=cfg["n_epochs_warmup"],
                shuffle=False,
                callbacks=[keras_callbacks],
                batch_size=cfg["batch_size"],
                verbose=cfg["verbose"],
            )

            warmup_eval_results = model.evaluate(
                test_loader,
                batch_size=cfg["batch_size"],
                verbose=cfg["verbose"],
            )

            cfg["n_mix_batches_per_epoch"] = orig_n_mix_batches_per_epoch

            print("Finished warmup.")
            print("Fitting now for another %d epochs ..." % (cfg["n_epochs"] - cfg["n_epochs_warmup"]))
        else:
            print("Fitting now for %d epochs ..." % cfg["n_epochs"])
            warmup_train_history, warmup_eval_results = None, None

        # now train remaining epochs
        train_files, test_files, val_files = split_into_train_test_val(cfg, deepcopy(batch_file_list))

        train_loader = ModelDataLoader(cfg, data, train_files)
        test_loader = ModelDataLoader(cfg, data, test_files)
        val_loader = ModelDataLoader(cfg, data, val_files)

        train_history = model.fit(
            train_loader,
            validation_data=val_loader,
            epochs=cfg["n_epochs"],
            shuffle=False,
            callbacks=[keras_callbacks],
            batch_size=cfg["batch_size"],
            initial_epoch=cfg["n_epochs_warmup"],
        )
        print("Finished fitting.")

        print("Evaluating model...")
        eval_results = model.evaluate(
            test_loader,
            batch_size=cfg["batch_size"],
            verbose=cfg["verbose"],
            return_dict=True,
        )
        pprint(eval_results)
        print("Evaluation finished, results above.")

        if warmup_train_history is not None:
            total_train_history = append_hist(warmup_train_history.history, train_history.history)
            total_train_history["warmup_eval_results"] = warmup_eval_results
            total_train_history["eval_results"] = eval_results
        else:
            total_train_history = train_history.history
            total_train_history["eval_results"] = eval_results

        # pack all information into total_train_history
        total_train_history["n_epochs_warmup"] = cfg["n_epochs_warmup"]
        total_train_history["n_epochs"] = cfg["n_epochs"]
        total_train_history["train_test_val_ratio"] = cfg["train_test_val_ratio"]
        total_train_history["batch_size"] = cfg["batch_size"]

        # save all results to a JSON file
        history_file = "models/history_%s.json" % cfg["time_string"]
        with open(history_file, "w") as fp:
            json.dump(total_train_history, fp, indent=4)

        print("Saved all training results to %s." % history_file)
    else:
        print("Training started. Batch learning disabled.")

        samples_batch = []
        for batch_file in batch_file_list:
            if batch_file.startswith("mix") or batch_file.startswith("base"):
                data_file = join(cfg["dir_dataset"], batch_file)
                with open(data_file, "rb") as opened_batch_file:
                    samples_batch.extend(pickle.load(opened_batch_file))

        # create input for CNN
        X_bottom, X_middle, X_top, y, _ = data.get_sample_data(samples_batch)

        print("Training started.")

        model.fit(
            {"bottom_input": X_bottom, "middle_input": X_middle, "top_input": X_top},
            y,
            epochs=cfg["n_epochs"],
            validation_split=cfg["train_test_val_ratio"][2],
            shuffle=False,
            callbacks=[keras_callbacks],
            batch_size=cfg["batch_size"],
            verbose=cfg["verbose"],
        )

    return model, keras_callbacks


def split_into_train_test_val(cfg, batch_file_list):
    train_test_val_ratio = cfg["train_test_val_ratio"]

    n_train_files = int(np.round(len(batch_file_list) * train_test_val_ratio[0]))
    n_test_files = int(np.round(len(batch_file_list) * train_test_val_ratio[1]))
    n_val_files = len(batch_file_list) - n_train_files - n_test_files

    train_files = list(np.random.choice(batch_file_list, n_train_files, replace=False))
    for f in train_files:
        batch_file_list.remove(f)

    test_files = list(np.random.choice(batch_file_list, n_test_files, replace=False))
    for f in test_files:
        batch_file_list.remove(f)

    val_files = batch_file_list

    return train_files, test_files, val_files


def append_hist(hist1, hist2):
    # create an empty dict to save all three history dicts into
    total_history_dict = dict()

    for some_key in hist1.keys():
        current_values = []  # to save values from all three hist dicts
        for hist_dict in [hist1, hist2]:
            current_values += hist_dict[some_key]
        total_history_dict[some_key] = current_values

    return total_history_dict