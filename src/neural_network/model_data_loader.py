from tensorflow.keras.utils import Sequence
from random import shuffle, choice
import numpy as np
import pickle
import os
from sklearn.preprocessing import StandardScaler
import glob
from copy import deepcopy
import librosa as lb
import random


class ModelDataGenerator(Sequence):
    """ Good resource: https://medium.com/analytics-vidhya/write-your-own-custom-data-generator-for-tensorflow-keras-1252b64e41c3
    """

    def __init__(self, cfg, instrument_list, n_steps_per_epoch, segment_length_in_s=0.3):
        """
        """
        self.cfg = cfg
        self.instrument_list = instrument_list
        self.n_steps_per_epoch = n_steps_per_epoch
        self.segment_length_in_s = segment_length_in_s

        self.current_step = 0

        self.batch_size = self.cfg["batch_size"]
        self.standardize = self.cfg["standardize_features"]

        if self.standardize:
            self.scaler = StandardScaler()
            print("Standardizing features to mean=0 and var=1.")
        else:
            print("Not standardizing features to mean=0 and var=1.")

        self.files = [[] * len(self.instrument_list)]

        for inst in self.instrument_list:
            cur_list = []

            cur_dir = os.path.join(self.cfg["dir_samples"], inst)
            if not os.path.isdir(cur_dir):
                raise RuntimeError("Directory %s does not exist!" % cur_dir)

            for file in glob.glob(cur_dir + "/*.wav"):
                if lb.get_duration(filename=file) > self.segment_length_in_s:
                    cur_list.append(file)

            self.files.append(deepcopy(cur_list))

        self.files.pop(0)

        self.prob_n_inst = [0.5, 0.2, 0.15, 0.15]

        a, _ = lb.load(self.files[0][0], sr=self.cfg["sr"], mono=True, offset=0.0, duration=self.segment_length_in_s)
        # we need to center, otherwise labels.shape[1] != envelope.shape[1]
        features = lb.vqt(a, sr=self.cfg["sr"], hop_length=self.cfg["hop_length"], fmin=27.5, n_bins=24 * 8, bins_per_octave=24)
        self.n_bins, self.n_frames_per_feature = features.shape

        self.n_samples_per_data_point = len(a)

        # we scale the envelope by that
        self.sliding_max = 1.0

    def __getitem__(self, index):
        """ Loads and returns one batch of features and labels.
        """
        n_inst_per_mix_max = len(self.prob_n_inst)
        n_inst_this_mix = np.random.choice(n_inst_per_mix_max, 1, p=self.prob_n_inst) + 1
        n_inst_this_mix = n_inst_this_mix[0]

        features = np.zeros((self.batch_size, self.n_bins, self.n_frames_per_feature, 1))
        labels = np.zeros((self.batch_size, self.n_frames_per_feature, 7))

        for i_data_in_batch in range(self.batch_size):
            if n_inst_this_mix > 1:
                inst_idx = np.random.choice(range(len(self.instrument_list)), n_inst_this_mix, replace=False)

                levels = np.random.normal(0.5, 0.1, size=n_inst_this_mix)
                levels /= np.sum(levels)

                for i_level, cur_inst in enumerate(inst_idx):
                    # get random path
                    file = np.random.choice(self.files[cur_inst])
                    new_audio = self._get_audio(file)

                    envelope = self._get_envelope(new_audio, self.cfg)
                    labels[i_data_in_batch, :, cur_inst] = envelope * levels[i_level]

                    if "audio" in locals():
                        audio += new_audio * levels[i_level]
                    else:
                        audio = new_audio

                vqt = self._get_features_for_audio(audio)
                features[i_data_in_batch, :, :, 0] = vqt
            else:
                inst_idx = np.random.choice(range(len(self.instrument_list)), 1, replace=False)[0]

                # get random path
                file = np.random.choice(self.files[inst_idx])
                audio = self._get_audio(file)

                envelope = self._get_envelope(audio, self.cfg)
                labels[i_data_in_batch, :, inst_idx] = envelope

                vqt = self._get_features_for_audio(audio)
                features[i_data_in_batch, :, :, 0] = vqt

        self.current_step += 1

        return features, labels

    def _get_audio(self, file):
        duration = lb.get_duration(filename=file)

        # TODO get offset at onset of instrument
        offset = np.max((duration - self.segment_length_in_s, 0.0))
        audio, _ = lb.load(file, sr=self.cfg["sr"], mono=True, offset=offset, duration=self.segment_length_in_s)

        return audio

    def _get_envelope(self, audio, cfg):
        BINS_PER_OCTAVE = 24
        F_MIN = 27.5
        N_BINS = BINS_PER_OCTAVE * 8

        vqt = lb.vqt(
            audio,
            sr=self.cfg["sr"],
            hop_length=self.cfg["hop_length"],
            fmin=F_MIN,
            n_bins=N_BINS,
            bins_per_octave=BINS_PER_OCTAVE,
            scale=True,
        )
        vqt = np.abs(vqt)

        envelope = np.sum(vqt, axis=0)
        self.sliding_max = np.max([np.max(envelope), self.sliding_max])
        envelope /= self.sliding_max

        return envelope

    def _get_features_for_audio(self, audio):
        features = lb.vqt(audio, sr=self.cfg["sr"], hop_length=self.cfg["hop_length"], fmin=27.5, n_bins=24 * 8, bins_per_octave=24)
        features = np.abs(features)

        return np.abs(features)

    def on_epoch_end(self):
        self.current_step = 0

    def __len__(self):
        return self.n_steps_per_epoch

    def _standardize(self, data):
        """ Standardizes data along its feature dimension (mostly features).

            We assume that the 2nd dimension is the feature dimension.
        """
        assert len(data.shape) == 4
        assert data.shape[-1] == 1

        for audio_file in range(data.shape[0]):
            data[audio_file, :, :, 0] = self.scaler.fit_transform(data[audio_file, :, :, 0].T).T

        return data


class ModelDataLoaderNew(Sequence):
    """ Good resource: https://medium.com/analytics-vidhya/write-your-own-custom-data-generator-for-tensorflow-keras-1252b64e41c3
    """

    def __init__(self, cfg,):
        """
        :param cfg:
        :param full_pickle_list: Equals batch_file_list
        :param pickle_type:
        :param need_to_reshape:
        :param model:
        """
        self.cfg = cfg

        self.pickle_base_dir = self.cfg["dir_dataset"]
        self.batch_size = self.cfg["batch_size"]
        self.standardize = self.cfg["standardize_features"]

        if self.standardize:
            self.scaler = StandardScaler()
            print("Standardizing features to mean=0 and var=1.")
        else:
            print("Not standardizing features to mean=0 and var=1.")

        self.pickle_paths = self._filter_pickle_paths()
        self.n_pickles = len(self.pickle_paths)
        self.n_frames_per_pickle = self._get_n_frames_per_pickle()

        self.n_batches_total = (self.n_frames_per_pickle * self.n_pickles) // self.batch_size

        print("Found %d batches in total, using a batch size of %d." % (self.n_batches_total, self.batch_size))

        if self.batch_size > self.n_batches_total:
            raise ValueError("Batch size must not be bigger then number of batches.")

        self.current_pickle_idx = 0
        self.batch_idx = 0
        self._load_two_pickles(initialize=True)

        self.data_counter = 0
        self.data_counter_enabled = False

    def __getitem__(self, index):
        """ Loads and returns one batch of features and labels.
        """
        start_idx = int(np.mod(self.batch_idx, self.n_frames_per_pickle))
        stop_idx = start_idx + self.batch_size

        features = self.features[start_idx:stop_idx, :, :, :]
        labels = self.labels[start_idx:stop_idx, :, :]

        # determine whether the end of our data reaches into the next pickle
        stop_pickle = int(
            np.floor((self.batch_idx + self.batch_size) / self.n_frames_per_pickle)
        )

        # if stop_pickle > self.current_pickle_idx we have reached into data of a new pickle
        # in that case load a new pickle
        if stop_pickle > self.current_pickle_idx:
            self.current_pickle_idx += 1
            self._load_two_pickles(initialize=False)

        # increment total counter
        self.batch_idx += self.batch_size

        if features.shape[0] == 0 or labels.shape[0] == 0:
            ValueError(
                "Received an empty batch at batch_idx "
                + str(self.batch_idx)
                + "."
            )

        return features, labels

    def on_epoch_end(self):
        random.shuffle(self.pickle_paths)

        # reset running indices and load two pickles to start with
        self.current_pickle_idx = 0
        self.batch_idx = 0
        self._load_two_pickles(initialize=True)

    def _load_two_pickles(self, initialize=False):
        """ Loads features and labels from two pickle files.

            The first pickle to load is at self.current_pickle_idx, the next one
            at self.current_pickle_idx + 1.

            If this is our first loading we load current and next pickle and concatenate them.
            If we loaded at least once we instead drop the first pickle, move the second one
            to the position of the first, and load a new one which we then append.
            This way we make sure that each pickle is only read once from the file system.
        """
        if initialize:
            # we have never loaded data until now
            with open(self.pickle_paths[self.current_pickle_idx], "rb") as file:
                features, labels = pickle.load(file)

            if self.standardize:
                features = self._standardize(features)

            # if there's only one pickle we can't load a second one
            if self.current_pickle_idx + 1 < self.n_pickles:
                with open(self.pickle_paths[self.current_pickle_idx + 1], "rb") as file:
                    features_2, labels_2 = pickle.load(file)

                if self.standardize:
                    features_2 = self._standardize(features_2)

                features = np.append(features, features_2, axis=0)
                labels = np.append(labels, labels_2, axis=0)

            self.features = features
            self.labels = labels
        else:
            # we have loaded at least once
            # this means that our previously 2nd pickle now becomes our first pickle
            self.features = self.features[self.n_frames_per_pickle:]
            self.labels = self.labels[self.n_frames_per_pickle:]

            # we load a new pickle and append it to the existing data
            # we only do this if there's a pickle left!
            if (self.current_pickle_idx + 1) < self.n_pickles:
                with open(self.pickle_paths[self.current_pickle_idx], "rb") as file:
                    loaded_features, loaded_labels = pickle.load(file)

                if self.standardize:
                    loaded_features = self._standardize(loaded_features)

                # here we need to append reshaped data, so we append to the batch-axis
                self.features = np.append(self.features, loaded_features, axis=0)
                self.labels = np.append(self.labels, loaded_labels, axis=0)

    def __len__(self):
        return self.n_batches_total

    def _filter_pickle_paths(self):
        """ Filters pickle paths into base and mix pickles.
        """
        files = []

        for f in glob.glob(os.path.join(self.pickle_base_dir, "*.pkl")):
            try:
                if "train_" in f:
                    files.append(str(f))
            except:
                print("invalid file ignored: {}".format(f))

        return files

    def _get_n_frames_per_pickle(self):
        """ Reads the data in the first pickle path and derives the number of frames per pickle from there.

            NOTE: Naturally this ModelDataLoader will fail if not all pickles have the same number of frames!
        """
        # read features and labels from first pickle path
        with open(self.pickle_paths[0], "rb") as file:
            features, labels = pickle.load(file)

        print("Found n_frames_per_pickle of %d." % features.shape[0])

        return features.shape[0]

    def _standardize(self, data):
        """ Standardizes data along its feature dimension (mostly features).

            We assume that the 2nd dimension is the feature dimension.
        """
        assert len(data.shape) == 4
        assert data.shape[-1] == 1

        for audio_file in range(data.shape[0]):
            data[audio_file, :, :, 0] = self.scaler.fit_transform(data[audio_file, :, :, 0].T).T

        return data


class ModelDataLoader(Sequence):
    """ Good resource: https://medium.com/analytics-vidhya/write-your-own-custom-data-generator-for-tensorflow-keras-1252b64e41c3
    """

    def __init__(self, cfg, data_object, full_pickle_list,):
        """
        :param cfg:
        :param full_pickle_list: Equals batch_file_list
        :param pickle_type:
        :param need_to_reshape:
        :param model:
        """
        self.cfg = cfg
        self.full_pickle_list = full_pickle_list
        self.data_object = data_object

        self.batch_size = self.cfg["batch_size"]
        self.standardize = self.cfg["standardize_features"]

        if self.standardize:
            self.scaler = StandardScaler()
            print("Standardizing features to mean=0 and var=1.")
        else:
            print("Not standardizing features to mean=0 and var=1.")

        self.all_base_pickles, self.all_mix_pickles = self._filter_pickle_paths()

        self.n_base_pickles_per_epoch = min(cfg["n_base_batches_per_epoch"], len(self.all_base_pickles))
        self.n_mix_pickles_per_epoch = min(cfg["n_mix_batches_per_epoch"], len(self.all_mix_pickles))
        self.n_pickles_per_epoch_total = self.n_base_pickles_per_epoch + self.n_mix_pickles_per_epoch

        print("Taking %d base pickles and %d mix pickles per epoch."
              % (self.n_base_pickles_per_epoch, self.n_mix_pickles_per_epoch))

        # get random list of base and mix pickles (mixed) for the 1st epoch
        self.chosen_pickles = self._get_random_pickles()

        self.n_frames_per_pickle = self._get_n_frames_per_pickle()

        # our number of available files during feature generation does not perfectly match
        # n_audio_files_per_batch
        # due to that, later batches may have a length smaller by 1 data sample
        # the biggest POSSIBLE error is -(n_files_per_pickle - 1)
        # this may lead to the last few batches not being used in the current epoch
        # however, as we shuffle our pickle paths between epochs, they are going to be used in the following epoch
        biggest_error = self.n_frames_per_pickle - 1
        # biggest_error = 0
        self.n_batches_total = (self.n_frames_per_pickle * self.n_pickles_per_epoch_total - biggest_error) \
                               // self.batch_size

        print("Found %d batches in total, using a batch size of %d." % (self.n_batches_total, self.batch_size))

        if self.batch_size > self.n_batches_total:
            raise ValueError("Batch size must not be bigger then number of batches.")

        self.current_pickle_idx = 0
        self.batch_idx = 0
        self._load_two_pickles(initialize=True)

        self.data_counter = 0
        self.data_counter_enabled = False

    def __getitem__(self, index):
        """ Loads and returns one batch of features and labels.
        """
        start_idx = int(np.mod(self.batch_idx, self.n_frames_per_pickle))
        stop_idx = start_idx + self.batch_size

        X_bottom = self.features["bottom_input"][start_idx:stop_idx, :, :, :]
        X_middle = self.features["middle_input"][start_idx:stop_idx, :, :, :]
        X_top = self.features["top_input"][start_idx:stop_idx, :, :, :]
        y = self.labels[start_idx:stop_idx, :, :]

        # determine whether the end of our data reaches into the next pickle
        stop_pickle = int(
            np.floor((self.batch_idx + self.batch_size) / self.n_frames_per_pickle)
        )

        # if stop_pickle > self.current_pickle_idx we have reached into data of a new pickle
        # in that case load a new pickle
        if stop_pickle > self.current_pickle_idx:
            self.current_pickle_idx += 1
            self._load_two_pickles(initialize=False)

        # increment total counter
        self.batch_idx += self.batch_size

        if X_bottom.shape[0] == 0 or y.shape[0] == 0:
            ValueError(
                "Received an empty batch at batch_idx "
                + str(self.batch_idx)
                + "."
            )

        if self.data_counter_enabled:
            self.data_counter += X_bottom.shape[0] * X_bottom.shape[1]

        return {
            "bottom_input": X_bottom,
            "middle_input": X_middle,
            "top_input": X_top
        }, y

    def on_epoch_end(self):
        self.chosen_pickles = self._get_random_pickles()

        # reset running indices and load two pickles to start with
        self.current_pickle_idx = 0
        self.batch_idx = 0
        self._load_two_pickles(initialize=True)

        # reset data_counter (exists only to check if all data was read during one epoch)
        if self.data_counter_enabled:
            print("Pickle type %s: number of data points in epoch: %d (n_batches x n_frames_per_batch)"
                     % (self.pickle_type, self.data_counter))
            self.data_counter = 0

    def _load_two_pickles(self, initialize=False):
        """ Loads features and labels from two pickle files.

            The first pickle to load is at self.current_pickle_idx, the next one
            at self.current_pickle_idx + 1.

            If this is our first loading we load current and next pickle and concatenate them.
            If we loaded at least once we instead drop the first pickle, move the second one
            to the position of the first, and load a new one which we then append.
            This way we make sure that each pickle is only read once from the file system.
        """
        if initialize:
            # we have never loaded data until now
            with open(self.chosen_pickles[self.current_pickle_idx], "rb") as file:
                pickle_data = pickle.load(file)

            X_bottom, X_middle, X_top, y, _ = self.data_object.get_sample_data(samples=pickle_data)

            if self.standardize:
                X_bottom = self._standardize(X_bottom)
                X_middle = self._standardize(X_middle)
                X_top = self._standardize(X_top)

            # if there's only one pickle we can't load a second one
            if self.current_pickle_idx + 1 < len(self.chosen_pickles):
                with open(self.chosen_pickles[self.current_pickle_idx + 1], "rb") as file:
                    pickle_data = pickle.load(file)

                X_bottom_2, X_middle_2, X_top_2, y_2, _ = self.data_object.get_sample_data(pickle_data)

                if self.standardize:
                    X_bottom_2 = self._standardize(X_bottom_2)
                    X_middle_2 = self._standardize(X_middle_2)
                    X_top_2 = self._standardize(X_top_2)

                X_bottom = np.append(X_bottom, X_bottom_2, axis=0)
                X_middle = np.append(X_middle, X_middle_2, axis=0)
                X_top = np.append(X_top, X_top_2, axis=0)
                y = np.append(y, y_2, axis=0)

            self.features = {
                "bottom_input": X_bottom,
                "middle_input": X_middle,
                "top_input": X_top,
            }
            self.labels = y
        else:
            # we have loaded at least once
            # this means that our previously 2nd pickle now becomes our first pickle
            self.features["bottom_input"] = self.features["bottom_input"][self.n_frames_per_pickle:]
            self.features["middle_input"] = self.features["middle_input"][self.n_frames_per_pickle:]
            self.features["top_input"] = self.features["top_input"][self.n_frames_per_pickle:]
            self.labels = self.labels[self.n_frames_per_pickle:]

            # we load a new pickle and append it to the existing data
            # we only do this if there's a pickle left!
            if (self.current_pickle_idx + 1) < len(self.chosen_pickles):
                with open(self.chosen_pickles[self.current_pickle_idx + 1], "rb") as file:
                    pickle_data = pickle.load(file)

                X_bottom, X_middle, X_top, y, _ = self.data_object.get_sample_data(samples=pickle_data)

                if self.standardize:
                    X_bottom = self._standardize(X_bottom)
                    X_middle = self._standardize(X_middle)
                    X_top = self._standardize(X_top)

                # here we need to append reshaped data, so we append to the batch-axis
                self.features["bottom_input"] = np.append(self.features["bottom_input"], X_bottom, axis=0)
                self.features["middle_input"] = np.append(self.features["middle_input"], X_middle, axis=0)
                self.features["top_input"] = np.append(self.features["top_input"], X_top, axis=0)
                self.labels = np.append(self.labels, y, axis=0)

    def __len__(self):
        return self.n_batches_total

    def _filter_pickle_paths(self):
        """ Filters pickle paths into base and mix pickles.
        """
        base_files, mix_files = [], []

        for f in self.full_pickle_list:
            try:
                if f.startswith("base"):
                    data_file = os.path.join(self.cfg["dir_dataset"], f)
                    base_files.append(str(data_file))

                if f.startswith("mix"):
                    data_file = os.path.join(self.cfg["dir_dataset"], f)
                    mix_files.append(str(data_file))
            except:
                print("invalid file ignored: {}".format(f))

        return base_files, mix_files

    def _get_random_pickles(self):
        """ Choose self.n_base_pickles_per_epoch random base pickles from self.all_base_pickles and
            self.n_mix_pickles_per_epoch random mix pickles from self.all_mix_pickles.
        """
        if len(self.all_base_pickles) > 0:
            self.random_base_pickles = np.random.choice(self.all_base_pickles, self.n_base_pickles_per_epoch, replace=False)
        else:
            self.random_base_pickles = []

        if len(self.all_mix_pickles) > 0:
            self.random_mix_pickles = np.random.choice(self.all_mix_pickles, self.n_mix_pickles_per_epoch, replace=False)
        else:
            self.random_mix_pickles = []

        chosen_pickles = list(self.random_base_pickles) + list(self.random_mix_pickles)
        shuffle(chosen_pickles)

        return chosen_pickles

    def _get_n_frames_per_pickle(self):
        """ Reads the data in the first pickle path and derives the number of frames per pickle from there.

            NOTE: Naturally this ModelDataLoader will fail if not all pickles have the same number of frames!
        """
        # read features and labels from first pickle path
        with open(self.chosen_pickles[0], "rb") as file:
            pickle_data = pickle.load(file)

        X_bottom, X_middle, X_top, y, _ = self.data_object.get_sample_data(samples=pickle_data)

        print("Found n_frames_per_pickle of %d." % X_bottom.shape[0])

        return X_bottom.shape[0]

    def _standardize(self, data):
        """ Standardizes data along its feature dimension (mostly features).

            We assume that the 2nd dimension is the feature dimension.
        """
        assert len(data.shape) == 4
        assert data.shape[-1] == 1

        for audio_file in range(data.shape[0]):
            data[audio_file, :, :, 0] = self.scaler.fit_transform(data[audio_file, :, :, 0].T).T

        return data
