import numpy as np
from sklearn.preprocessing import StandardScaler

from src.pre_processing.dataset import Sample


def predict_data_fail(cfg, model, beats, buffer_len, audio_buffer, keras_callbacks):
    if len(beats) == 0 or buffer_len == 0 or cfg["num_instruments"] == 0:
        print("Warning: Min. 1 of input dimension values for y_pred is 0")

    if cfg["standardize_features"]:
        scaler = StandardScaler()
        print("Standardizing features before prediction to mean=0 and var=1.")
    else:
        print("Not standardizing features for prediction.")

    y_pred = np.zeros((len(beats), buffer_len, cfg["num_instruments"]))

    i_batch_element = -1

    # we need to save all tat and ii values until we save predictions to y
    tat_list, ii_list = [], []

    i_beat = -1

    for tat, ii in enumerate(beats):
        X_pred = Sample.extract_features(audio_buffer[:, ii], cfg)

        i_beat += 1

        # Prediction using the CNN
        if ii + cfg["n_frames"] < buffer_len:
            i_batch_element += 1
            tat_list.append(tat)
            ii_list.append(ii)

            if i_beat == 0:
                # create new batch
                x_bottom = np.expand_dims(
                    np.expand_dims(X_pred[0], axis=0), axis=-1
                )
                x_middle = np.expand_dims(
                    np.expand_dims(X_pred[1], axis=0), axis=-1
                )
                x_top = np.expand_dims(np.expand_dims(X_pred[2], axis=0), axis=-1)
            else:
                # append to existing batch
                x_bottom_new = np.expand_dims(
                    np.expand_dims(X_pred[0], axis=0), axis=-1
                )
                x_middle_new = np.expand_dims(
                    np.expand_dims(X_pred[1], axis=0), axis=-1
                )
                x_top_new = np.expand_dims(np.expand_dims(X_pred[2], axis=0), axis=-1)

                x_bottom = np.concatenate((x_bottom, x_bottom_new))
                x_middle = np.concatenate((x_middle, x_middle_new))
                x_top = np.concatenate((x_top, x_top_new))

        # else:
        #     break

    x = {
        "bottom_input": x_bottom,
        "middle_input": x_middle,
        "top_input": x_top,
    }

    predictions = model.predict(
        x=x,
        callbacks=[keras_callbacks],
        verbose=0,
        batch_size=cfg["batch_size"],
    )
    # predictions = np.squeeze(predictions)
    # predictions = predictions.T

    # for it in range(predictions.shape[2]):
    #     # y_pred[tat, ii : ii + cfg["n_frames"], :] = predictions
    #     y_pred[tat_list[it], ii_list[it] : ii_list[it] + cfg["n_frames"], :] = predictions[:, :, it]


    return predictions


def predict_data(cfg, model, beats, buffer_len, audio_buffer, keras_callbacks):
    if len(beats) == 0 or buffer_len == 0 or cfg["num_instruments"] == 0:
        print("Warning: Min. 1 of input dimension values for y_pred is 0")

    if cfg["standardize_features"]:
        scaler = StandardScaler()
        print("Standardizing features before prediction to mean=0 and var=1.")
    else:
        print("Not standardizing features for prediction.")

    y_pred = np.zeros((len(beats), buffer_len, cfg["num_instruments"]))

    i_batch_element = -1

    for tat, ii in enumerate(beats):
        X_pred = Sample.calc_stacked_cqt(audio_buffer[:, ii], cfg)
        # Prediction using the CNN
        if ii + cfg["n_frames"] < buffer_len:
            i_batch_element += 1

            x_bottom = np.expand_dims(
                np.expand_dims(X_pred[0], axis=0), axis=-1
            )
            x_middle = np.expand_dims(
                np.expand_dims(X_pred[1], axis=0), axis=-1
            )
            x_top = np.expand_dims(np.expand_dims(X_pred[2], axis=0), axis=-1)

            if cfg["standardize_features"]:
                for it in range(x_bottom.shape[0]):
                    # standardize accross feature dimension
                    x_bottom[it, :, :, 0] = scaler.fit_transform(x_bottom[it, :, :, 0].T).T
                    x_middle[it, :, :, 0] = scaler.fit_transform(x_middle[it, :, :, 0].T).T
                    x_top[it, :, :, 0] = scaler.fit_transform(x_top[it, :, :, 0].T).T

            x = {
                "bottom_input": x_bottom,
                "middle_input": x_middle,
                "top_input": x_top,
            }

            if cfg["model_to_create"] == "dense_net":
                # Dense_net needs to have all 3 CQT ranges stacked into 1 array
                X_bottom = np.tile(x["bottom_input"], (1, 1, 1, 3))
                X_middle = np.tile(x["middle_input"], (1, 1, 1, 3))
                X_top = np.tile(x["top_input"], (1, 1, 1, 3))
                x = np.concatenate((X_bottom, X_middle, X_top), axis=1)

            predictions = model.predict(
                x=x,
                callbacks=[keras_callbacks],
                verbose=0,
            )
            predictions = np.squeeze(predictions)
            predictions = predictions.T
            y_pred[tat, ii: ii + cfg["n_frames"], :] = predictions

        else:
            break

    return y_pred



def predict_data_old(cfg, model, beats, buffer_len, audio_buffer, keras_callbacks):
    if len(beats) == 0 or buffer_len == 0 or cfg["num_instruments"] == 0:
        print("Warning: Min. 1 of input dimension values for y_pred is 0")

    if cfg["standardize_features"]:
        scaler = StandardScaler()
        print("Standardizing features before prediction to mean=0 and var=1.")
    else:
        print("Not standardizing features for prediction.")

    y_pred = np.zeros((len(beats), buffer_len, cfg["num_instruments"]))

    i_batch_element = -1

    # we need to save all tat and ii values until we save predictions to y
    tat_list, ii_list = [], []

    for tat, ii in enumerate(beats):
        X_pred = Sample.calc_stacked_cqt(audio_buffer[:, ii], cfg)
        # Prediction using the CNN
        if ii + cfg["n_frames"] < buffer_len:
            i_batch_element += 1
            tat_list.append(tat)
            ii_list.append(ii)

            if np.mod(i_batch_element, cfg["batch_size"]) == 0:
                i_batch_element = 0

                # create new batch
                x_bottom = np.expand_dims(
                    np.expand_dims(X_pred[0], axis=0), axis=-1
                )
                x_middle = np.expand_dims(
                    np.expand_dims(X_pred[1], axis=0), axis=-1
                )
                x_top = np.expand_dims(np.expand_dims(X_pred[2], axis=0), axis=-1)
            else:
                # append to existing batch
                x_bottom_new = np.expand_dims(
                    np.expand_dims(X_pred[0], axis=0), axis=-1
                )
                x_middle_new = np.expand_dims(
                    np.expand_dims(X_pred[1], axis=0), axis=-1
                )
                x_top_new = np.expand_dims(np.expand_dims(X_pred[2], axis=0), axis=-1)

                x_bottom = np.concatenate((x_bottom, x_bottom_new))
                x_middle = np.concatenate((x_middle, x_middle_new))
                x_top = np.concatenate((x_top, x_top_new))

            # check if we reached our batch size and if yes predict
            if x_bottom.shape[0] == cfg["batch_size"]:
                if cfg["standardize_features"]:
                    for it in range(x_bottom.shape[0]):
                        # standardize accross feature dimension
                        x_bottom[it, :, :, 0] = scaler.fit_transform(x_bottom[it, :, :, 0].T).T
                        x_middle[it, :, :, 0] = scaler.fit_transform(x_middle[it, :, :, 0].T).T
                        x_top[it, :, :, 0] = scaler.fit_transform(x_top[it, :, :, 0].T).T

                x = {
                    "bottom_input": x_bottom,
                    "middle_input": x_middle,
                    "top_input": x_top,
                }

                if cfg["model_to_create"] == "dense_net":
                    # Dense_net needs to have all 3 CQT ranges stacked into 1 array
                    X_bottom = np.tile(x["bottom_input"], (1, 1, 1, 3))
                    X_middle = np.tile(x["middle_input"], (1, 1, 1, 3))
                    X_top = np.tile(x["top_input"], (1, 1, 1, 3))
                    x = np.concatenate((X_bottom, X_middle, X_top), axis=1)

                predictions = model.predict(
                    x=x,
                    callbacks=[keras_callbacks],
                    verbose=0,
                    batch_size=cfg["batch_size"],
                )
                predictions = np.squeeze(predictions)
                predictions = predictions.T
                for it in range(predictions.shape[2]):
                    # y_pred[tat, ii : ii + cfg["n_frames"], :] = predictions
                    y_pred[tat_list[it], ii_list[it] : ii_list[it] + cfg["n_frames"], :] = predictions[:, :, it]

                # reset the index list for the next batch
                tat_list, ii_list = [], []

        else:
            break

    return y_pred
