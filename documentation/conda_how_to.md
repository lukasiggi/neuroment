Add a new package to neuroment environment:
  - conda install -name neuroment numpy=1.19.2

List available packages for package name:
  - conda search numpy

Export activated environment to environment.yml
- conda env export > environment.yml

